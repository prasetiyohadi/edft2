#include "sch.h"

const double pi = 3.141592653589793238462643383279502884197169399375105820974\
9445923078164062862089986280348253421170679;

Sch::Sch(const COperator &_op, const int &_Ns): op(_op), ns(_Ns), s(_op.s),
	V_shm(2*_op.idx.dr.Square())
{}

CVector Sch::Vdual() const
{
	return op.cJdag(op.O(op.cJ(V_shm)));
}

double Sch::getE(const CMatrix &W) const
{
	CMatrix U = zgemm(complex_double(1.), W.ConjTranspose(), op.O(W));
	CVector n = diagouter(zgemm(complex_double(1.), op.cI(W), inverse(U)),
			op.cI(W));
	CMatrix E = zgemm(complex_double(1.), W.ConjTranspose(), 
			zgemm(complex_double(1.), op.L(W), inverse(U)));
	return (trace(E)*complex_double(-1.,0.)/complex_double(2.,0.)
			+ zdotc(s, Vdual(), n)).real();
}

CMatrix Sch::getGrad(const CMatrix &W) const
{
	complex_double a = complex_double(1.);
	CMatrix U = zgemm(a, W.ConjTranspose(), op.O(W));
	CMatrix grad = H(W);
	grad = zgemm(-a, zgemm(a, op.O(W), inverse(U)), zgemm(a, W.ConjTranspose(),
				H(W)), a, grad);
	grad = zgemm(a, grad, inverse(U));
	return grad;
}

CMatrix Sch::H(const CMatrix &W) const
{
	CMatrix Hw = zgemm(complex_double(-1./2.), op.L(W), zidm(ns));
	Hw = zgemm(complex_double(1.), op.cIdag(diagprod(Vdual(), op.cI(W))),
			zidm(ns), complex_double(1.), Hw);
	return Hw;
}

void Sch::fdtest(const CMatrix &W) const
{
	double dE = 0.;
	double E0 = getE(W);
	complex_double z = complex_double(1.0);
	CMatrix g0 = getGrad(W);
	CMatrix dW(s, ns); dW.Randomize();
	std::cout << "initial energy: " << E0 << "\n";
	for (double a=10; a>1e-9; a*=1e-1)
	{
		dE = 2*trace(zgemm(a*z, g0.ConjTranspose(), dW)).real();
		std::cout.precision(17);
		std::cout << "ratio actual change to expected change energy:\t"
			<< std::fixed
			<< (getE(zgemmcpy(a*z, dW, zidm(ns), z, W))-E0)/dE
			<< "\n";
		std::cout << "estimate of the error due to rounding:\t\t"
			<< std::fixed
			<< std::sqrt(s)*doubleEps()/std::fabs(dE)
			<< "\n\n";
	}
}

void Sch::getPsi(const CMatrix &W, CMatrix &Psi, Vector &eps)
{
	CMatrix mu = zgemm(complex_double(1.,0.), W.ConjTranspose(), H(W));
	CVector zeps(eps);
	Eig(mu, Psi, zeps);
	eps = zeps.Real();
}

void Sch::sd(CMatrix &W, const int t) const
{
	const double alpha = 3.0e-5;
	for (int i=0; i<t; ++i)
	{
		W = zgemm(complex_double(-1.*alpha), getGrad(W),
				zidm(ns), complex_double(1.), W);
		std::cout << "E = " << getE(W) << "\n";
	}
}
