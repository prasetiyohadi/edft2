#include "ewald.h"

const double pi = 3.141592653589793238462643383279502884197169399375105820974\
9445923078164062862089986280348253421170679;

Ewald::Ewald(const COperator &_op, const Matrix &_X): op(_op), s(_op.s), X(_X)
{}

Vector Ewald::rn(const Vector &g) const
{
	return op.cI(zgbvv(s, s, complex_double(1.), op.cJ(g),
				op.idx.structureFactor(X))).Real();
}

Vector Ewald::rphi(const Vector &g) const
{
	return op.cI(op.Linv(-4.0*pi*op.O(op.cJ(rn(g))))).Real();
}	

double Ewald::Unum(const Vector &g) const
{
	return 0.5*zdotc(s, op.cJ(rphi(g)), op.O(op.cJ(rn(g)))).real();
}
