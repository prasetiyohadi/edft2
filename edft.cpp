//Memasukkan include paket

#include "edft.h"
#include "package.h"
#include "out.h"
#include "opmat.h"
#include "setup.h"
#include "poisson.h"
#include "ewald.h"


//c-Memasukkan definisi
#define PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679

//c-Program utama
int main()
{
 //c-Cara cepat untuk mendefinisikan vektor dan matrik 
 typedef std::vector<int> vector_int; 
 typedef std::vector<double> vector_double;
 typedef std::vector<vector_int> vector_vector;
 typedef std::vector<vector_double> vector_vector_double;
 typedef std::vector<vector_vector_double> tensor_double;
 typedef std::vector<vector_vector> tensor_int;
 
 //o-S=[3,4,5] 
 vector_int S(3); //c-Definisi %S merupakan vektor integer
 S[0]=20;
 S[1]=25;
 S[2]=30;
 int batas=pac_prod(S);
 //c-Memasukkan %latice vector untuk menghitung matriks Diagonal.
 vector_int Latice(3); //c-Definisi %Lattice vektor integer
 Latice[0]=6;
 Latice[1]=6;
 Latice[2]=6;

 double Z=1.;
 
 std::vector<double> r(batas*3),g2(batas),g(batas*3); //c-inisiasi vektor r dan g.

 set_setup(S,Latice,&r[0],&g2[0],&g[0]); //c-menghitung r dan g, dari paket setup
// out_dm(opmat_vtm(g,batas,3),batas,3);

 vector_vector_double vec(batas);
 set_dplot(g2,S);
 
 std::vector<std::vector<int> > R=pac_Diag(Latice);

 std::cout<<"package -- poisson \n";

 poisson(r,S,R,g2);

 std::vector<double> X(6),gx(batas*2); //c-%X adalah posisi atom 
 for (int i=0;i<6;i++)
   X[i]=0.; X[3]=1.75;

 double complex *Sf;
 Sf=(double complex*) malloc(sizeof (double complex)*batas);
 //c-ewald_Sf 
 ewald_Sf(&g[0],X,batas,&Sf[0]);

/* for(int i=0;i<batas;i++)
    {
     std::cout<<creal(Sf[i])<<" "<<cimag(Sf[i])<<"\n";
    }*/

 std::cout<<"Package -- ewald \n"; 
 ewald(S,r,R,Z,&Sf[0],g2);
// out_dm(opmat_vtm(r,batas,3),batas,3); 
 return 0;

free(Sf);
}
