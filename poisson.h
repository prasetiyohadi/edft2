#ifndef _POISSON_H
#define _POISSON_H

#include "edft2.h"
#include "setup.h"
#include <fftw3.h>

struct Operator
{
	int m, s, x, y, z;
	double det;
	Index idx;
	Operator(const Index &_idx);
	complex_double *O(complex_double *in);
	complex_double **O(complex_double **in, const int &ns);
	complex_double *L(complex_double *in);
	complex_double **L(complex_double **in, const int &ns);
	complex_double *Linv(complex_double *in);
	complex_double **Linv(complex_double **in, const int &ns);
	complex_double *fft3(complex_double *in, const int &f);
	complex_double *cJ(const Vector &n);
	complex_double **cJ(const Matrix &n, const int &ns);
	complex_double *cJdag(complex_double *in);
	complex_double **cJdag(complex_double **in, const int &ns);
	complex_double *cI(complex_double *in);
	complex_double **cI(complex_double **in, const int &ns);
	complex_double *cIdag(complex_double *in);
	complex_double **cIdag(complex_double **in, const int &ns);
};

struct COperator
{
	int m, s, x, y, z;
	double det;
	Index idx;
	COperator(const Index &_idx);
	CVector O(const CVector &in) const;
	CMatrix O(const CMatrix &in) const;
	CVector L(const CVector &in) const;
	CMatrix L(const CMatrix &in) const;
	CVector Linv(const CVector &in) const;
	CMatrix Linv(const CMatrix &in) const;
	CVector fft3(const CVector &in, const int f) const;
	CVector cJ(const Vector &n) const;
	CMatrix cJ(const Matrix &n) const;
	CVector cJdag(const CVector &in) const;
	CMatrix cJdag(const CMatrix &in) const;
	CVector cI(const CVector &in) const;
	CMatrix cI(const CMatrix &in) const;
	CVector cIdag(const CVector &in) const;
	CMatrix cIdag(const CMatrix &in) const;
};

struct Poisson
{
	int s;
	COperator op;
	//Operator op;
	Poisson(const COperator &_op);
	Vector phi(const Vector &n) const;
	Matrix phi(const Matrix &n) const;
	double Unum(const Vector &n) const;
	double Unum(const Matrix &n) const;
};

#endif
