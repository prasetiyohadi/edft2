#include "setup.h"

const double pi = 3.141592653589793238462643383279502884197169399375105820974\
9445923078164062862089986280348253421170679;

Index::Index(const Matrix &_R, const Vector &_S): s(_S.Product()),
	dim(_S.Size()), S(_S), R(_R)
{
	dr.Resize(s);
	G.Resize(s, dim);
	G2.Resize(s);
	M.Resize(s, dim);
	N.Resize(s, dim);
	r.Resize(s, dim);
	M = initM();
	N = initN();
	r = M*invDiagMat(diagMat(S))*R.Transpose();
	dr = centerDistance();
	G = 2.0*pi*N*R.Inverse();
	G2 = squareMagnitude(G);
}

Vector Index::centerDistance() const
{
	int s1 = s;
	int s2 = dim;
	Vector ones(s1), result(s1), v(s2);
	ones.Initialize(1.0);
	Matrix tmp = (r - (ones*R.Transpose().RowSum()/2)).Transpose();
	for (int i=0; i<s1; i++)
	{
		tmp.GetColumn(i,v);
		result(i) = v.Length();
	}
	return result;
}

Vector Index::squareMagnitude(const Matrix &G) const
{
	int s1 = G.Rows();
	int s2 = G.Columns();
	Vector result(s1), v(s2);
	Matrix tmp = G.Transpose();
	for (int i=0; i<s1; i++)
	{
		tmp.GetColumn(i,v);
		result(i) = v.SquareLength();
	}
	return result;
}

CVector Index::structureFactor(const Matrix &X) const
{
	CVector result(s);
	Matrix tmp = G*X.Transpose();
	for (int i=0; i<s; ++i)
		for (int j=0; j<X.Rows(); ++j)
			result(i) += std::exp(complex_double(0.,tmp(i,j)));
	return result;
}

Matrix Index::initM() const
{
	int rows = s;
	int columns = dim;
	Vector ms(rows);
	Matrix M(rows, columns);
    for (int i=0; i<rows; i++) ms(i)=i;
    for (int i=0; i<rows; i++)
    {
        M(i,0)=(int)ms(i)%(int)S(0);
        M(i,1)=(int)std::floor(ms(i)/S(0))%(int)S(1);
        M(i,2)=(int)std::floor(ms(i)/(S(0)*S(1)))%(int)S(2);
    }

	return M;
}

Matrix Index::initN() const
{
	int rows = s;
	int columns = dim;
	int tmp;
	Matrix N = M;
	for (int i=0; i<rows; i++)
		for (int j=0; j<columns; j++)
		{
			tmp = (N(i,j)>S(j)/2)?1:0;
			N(i,j) = N(i,j) - tmp*S(j);
		}
	return N;
}

Density::Density(const Index &_idx, const int &_Z, const double &_sigma):
	idx(_idx), Z(_Z), sigma(_sigma)
{
	g.Resize(idx.s);
	g = Z*gaussianDistrib(idx.dr, sigma);
}

Density::Density(const Index &_idx, const double &_sigma1,
		const double &_sigma2): idx(_idx), sigma1(_sigma1), sigma2(_sigma2)
{
	g1.Resize(idx.s);
	g2.Resize(idx.s);
	n.Resize(idx.s);
	g1 = gaussianDistrib(idx.dr, sigma1);
	g2 = gaussianDistrib(idx.dr, sigma2);
	n = g2 - g1;
}

double Density::Uanal() const
{
	return ((1./sigma1+1./sigma2)/2.
			-std::sqrt(2.)/sqrt(sigma1*sigma1+sigma2*sigma2)
			)/std::sqrt(pi);
}

double Density::Uself(const Matrix &X) const
{
	return Z*Z/(2.*std::sqrt(pi))*(1./sigma)*X.Rows();
}

Vector Density::gaussianDistrib(const Vector &dr, const double &sigma) const
{
	return (-dr.Square()/(2*sigma*sigma)).Exp()/std::sqrt(std::pow(
				2*pi*sigma*sigma,3.0));
}
