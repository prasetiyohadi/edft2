#include "test.h"
#include "setup.h"
#include "utils.h"
#include "datatypes.h"

int main()
{
	int r = 3, c = 3;

	cout << "Identity matrix I:";
	double** Id = new double*[r];
	for (int i=0; i<r; ++i) Id[i] = new double[c];
	Id[0][0] = 1.; Id[0][1] = 0.; Id[0][2] = 0.;
	Id[1][0] = 0.; Id[1][1] = 1.; Id[1][2] = 0.;
	Id[2][0] = 0.; Id[2][1] = 0.; Id[2][2] = 1.;
	Matrix I(r, c, Id);
	I.Print();
	for (int i=0; i<r; ++i) delete[] Id[i];
	delete[] Id;

	cout << "C matrix :";
	double** cm = new double*[r];
	for (int i=0; i<r; ++i) cm[i] = new double[c];
	cm[0][0] = 2.; cm[0][1] = -1.; cm[0][2] = -2.;
	cm[1][0] = -4.; cm[1][1] = 6.; cm[1][2] = 3.;
	cm[2][0] = -4.; cm[2][1] = -2.; cm[2][2] = 8.;
	Matrix lu(r,c), L(r,c), U(r,c), C(r, c, cm);
	C.Print();
	for (int i=0; i<r; ++i) delete[] cm[i];
	delete[] cm;

	cout << "D matrix :";
	double **dm = new double*[r];
	for (int i=0; i<r; ++i) dm[i] = new double[c];
	dm[0][0] = 1.; dm[0][1] = -3.; dm[0][2] = 1.;
	dm[1][0] = 2.; dm[1][1] = -5.; dm[1][2] = 4.;
	dm[2][0] = 2.; dm[2][1] = -2.; dm[2][2] = 11.;
	Matrix D(r, c, dm), Dl(r,c), Du(r,c), Dlu(r, c);
	D.Print();
	for (int i=0; i<r; ++i) delete[] dm[i];
	delete[] dm;

	cout << "E matrix:";
	double **em = new double*[r];
	for (int i=0; i<r; ++i) em[i] = new double[c];
	em[0][0] = 6.; em[0][1] = 0.; em[0][2] = 0.;
	em[1][0] = 0.; em[1][1] = 6.; em[1][2] = 0.;
	em[2][0] = 0.; em[2][1] = 0.; em[2][2] = 6.;
	Matrix E(r, c, em), Elu(r, c), Einv(r, c);
	E.Print();
	for (int i=0; i<r; ++i) delete[] em[i];
	delete[] em;

	/*cout << "Vector a :";
	double *out = new double[r];
	out[0] = 0.; out[1] = 5; out[2] = 22;
	Vector a(r, out);
	a.Print();
	delete[] out;*/

	cout << "Vector d :";
	Vector d(r);
	I.GetColumn(0, d);
	d.Print();

	cout << "LU decomposition of C matrix :\n";
	C.LUdecomp(L, U);
	cout << "Matrix L:";
	L.Print();
	cout << "Matrix U:";
	U.Print();

	/*cout << "Solve vector b for D.b = a :";
	Vector b(r);
	D.Solve(b, a);
	b.Print();*/

	cout << "LU decomposition of D matrix :\n";
	D.LUdecomp(Dl, Du);
	cout << "Matrix L:";
	Dl.Print();
	cout << "Matrix U:";
	Du.Print();
	cout << "Matrix LU:\n";
	Matrix Pd(r, c);
	double pvd;
	D.LUdecomp(Dlu, Pd, pvd);
	Dlu.Print();

	cout << "determinant of D matrix = " << D.Det() << "\n";

	cout << "LU decomposition of E matrix :\n";
	cout << "Matrix LU:\n";
	Matrix Pe(r, c);
	double pve;
	E.LUdecomp(Elu, Pe, pve);
	Elu.Print();

	cout << "Solve vector e for E.e = d :";
	Vector e(r);
	E.Solve(e, d);
	e.Print();

	cout << "Inverse of E matrix:\n";
	Einv = E.Inverse();
	Einv.Print();

	cout << "determinant of E matrix = " << E.Det() << "\n";

	const int s = 1000;

	cout << "\nPOD Instances single copy time\n";

	double** A = new double*[s];
	for (int i=0; i<s; ++i) A[i] = new double[s];

    clock_t RowMajor1 = clock();
    for(int i = 0; i < s; ++i)
        for(int j = 0; j < s; ++j)
            A[i][j]++;

    double trow1 = static_cast<double>(clock() - RowMajor1) / CLOCKS_PER_SEC;

	for (int i=0; i<s; ++i) delete[] A[i];
	delete[] A;

    cout << "Row-Major: " << trow1;

	double** B = new double*[s];
	for (int i=0; i<s; ++i) B[i] = new double[s];

    clock_t ColMajor1 = clock();
    for(int j = 0; j < s; ++j)
        for(int i = 0; i < s; ++i)
            B[i][j]++;

    double tcol1 = static_cast<double>(clock() - ColMajor1) / CLOCKS_PER_SEC;

	for (int i=0; i<s; ++i) delete[] B[i];
	delete[] B;

    cout << "\tColumn-Major: " << tcol1;
	cout << "\n";

	cout << "Class Instances single copy time\n";

	Matrix M(s,s);
    clock_t RowMajor2 = clock();
    for(int i = 0; i < s; ++i)
        for(int j = 0; j < s; ++j)
            M(i,j)++;
    double trow2 = static_cast<double>(clock() - RowMajor2) / CLOCKS_PER_SEC;

    cout << "Row-Major: " << trow2;

	Matrix Nm(s,s);
    clock_t ColMajor2 = clock();
    for(int j = 0; j < s; ++j)
        for(int i = 0; i < s; ++i)
            Nm(i,j)++;
    double tcol2 = static_cast<double>(clock() - ColMajor2) / CLOCKS_PER_SEC;

    cout << "\tColumn-Major: " << tcol2;
	cout << "\n";

	//double **o = new double*[r];
	//for (int i=0; i<r; ++i) o[i] = new double[c];
	//o[0][0] = 1.;		o[0][1] = 1./2.;	o[0][2] = 1./3.;
	//o[1][0] = 1./2.;	o[1][1] = 1./3.;	o[1][2] = 1./4.;
	//o[2][0] = 1./3.;	o[2][1] = 1./4.;	o[2][2] = 1./5.;
	double o[][6] = 
	{
		{ 1.,    1./2., 1./3., 1./4., 1./5.,  1./6.  },
		{ 1./2., 1./3., 1./4., 1./5., 1./6.,  1./7.  },
		{ 1./3., 1./4., 1./5., 1./6., 1./7.,  1./8.  },
		{ 1./4., 1./5., 1./6., 1./7., 1./8.,  1./9.  },
		{ 1./5., 1./6., 1./7., 1./8., 1./9.,  1./10. },
		{ 1./6., 1./7., 1./8., 1./9., 1./10., 1./11. },
	};
	//Matrix O(r, c, o); O.Print();
	Matrix O(o); O.Print();
	cout << "Hessenberg matrix O = ";
	Hessenberg(O); O.Print();
	/*Matrix orm(r);
	Vector ox(c);
	for (int k=0; k<c-1; ++k)
	{
		O.GetColumn(k, ox); ox.Print();
		Vector oxt = ox.Offset(k);
		Vector owt = oxt;
		double alpha = HouseholderTrans(oxt, owt); owt.Print();
		std::cout << "alpha(" << k << ") = " << alpha << "\n";
		orm(k,k) = alpha;
		double beta = 2/dot(owt, owt);
		std::cout << "beta(" << k << ") = " << beta << "\n";
		for (int i=1; i<c; ++i) O(i,k) = 0.;
		for (int j=1; j<c; ++j)
		{
		oj(j) = 0;
		for (int q=0; q<r; ++q)
		{
			oj(j) = oj(j)+beta1*ow(q)*O(q,j);
		}
		std::cout << "oj(" << j << ") = " << oj(j) << "\n";
		for (int i=0; i<r; ++i)
		{
			orm(i,j) = O(i,j)-oj(j)*ow(i);
			O(i,j) = orm(i,j);
		}
		orm.Print();
		O.Print();
	}
	O.GetColumn(1, ox); ox.Print();
	Vector oxt = ox.Offset(1); oxt.Print();
	Vector owt = oxt;
	oa = HouseholderTrans(oxt, owt); owt.Print();
	std::cout << "oa = " << oa << "\n";
	orm(1,1) = oa;
	double beta = 2/dot(owt, owt);
	std::cout << "beta = " << beta << "\n";
	for (int i=2; i<c; ++i) O(i,1) = 0.;
	for (int j=2; j<c; ++j)
	{
		oj(j) = 0.;
		for (int q=1; q<r; ++q)
		{
			oj(j) = oj(j)+beta*owt(q-1)*O(q,j);
		}
		std::cout << "oj(" << j << ") = " << oj(j) << "\n";
		for (int i=1; i<r; ++i)
		{
			orm(i,j) = O(i,j)-oj(j)*owt(i-1);
			O(i,j) = orm(i,j);
		}
		orm.Print();
		O.Print();
	}*/
	std::cout << "qr test end\n";

	double in[][3] = {
		{ 12, -51,   4},
		{  6, 167, -68},
		{ -4,  24, -41},
	};
	Matrix P(in); P.Print();
	cout << "Hessenberg matrix P = ";
	Hessenberg(P); P.Print();

	double _va[3] = {1, 2, -3};
	Vector va(_va); va.Print();
	Vector vaw(va.Size());
	double vaa = HouseholderTrans(va, vaw);
	std::cout << "vaa = " << vaa << "\n";
	cout << "Household vaw = "; vaw.Print();

	double y[][4] = {
		{ 4.61466,  0.79301,  0.00000,  0.00000},
		{ 0.79301,  3.38957,  0.01564,  0.00000},
		{ 1.00000,  0.01564,  5.01720,  0.00083},
		{ 2.00000,  0.00000,  0.00083,  5.00063},
	};
	/*double y[][3] = {
		{  2.00000, -4.58258,  0.00000},
		{ -4.58258,  1.09524, -1.08692},
		{  0.00000, -1.08692,  2.90476},
	};*/
	Matrix Y(y); Y.Print();

	const int N = 4;
	//const int N = 3;
	Vector a(N), b(N), lambda(N), p(N);
	Matrix Q(N);

	/*for(int i=0;i<N;++i){
		a(i) = 1.5 + i;
		b(i) = 0.3;
		p(i) = i;
	}*/
	for (int i=0; i<N; ++i)
	{
		//a(i) = O(i,i);
		//b(i) = O(i%(r-1)+1,i%(r-1));
		a(i) = Y(i,i);
		b(i) = Y(i%(N-1)+1,i%(N-1));
		p(i) = i;
	}
	a.Print(); b.Print(); p.Print();
	double *ap = a.GetPointer();
	double *bp = b.GetPointer();
	//Swap(&ap[1], &ap[3]);
	TDQREigensolver(N, a.GetPointer(), b.GetPointer(), lambda.GetPointer(), Q);
	//TDQREigensolver(N, a.GetPointer(), b.GetPointer(), lambda.GetPointer(),
	//		Q.GetPointer());

	std::cout.precision(17);
	std::cout << "Q matrix = "; Q.Print();
	cout << "The eigenvalues are: " << endl;
	lambda.Print();
	Matrix A1 = Q*diagMat(lambda)*Q.Transpose(); A1.Print();
	
	complex_double z[][4] = 
	{
		{
			complex_double(4.61466096987939078,-0.00000000000000033),
			complex_double(-0.21686561480329894,-0.14072594571898364),
			complex_double(0.53054595023796780,0.32913427036262255),
			complex_double(0.41326818461964887,-0.03784292478458216)
		},
		{
			complex_double(-0.21686561480329866,0.14072594571898545),
			complex_double(4.84381185226941380,-0.00000000000000028),
			complex_double(0.41491987965405497,-0.00916903178669083),
			complex_double(0.20582764445535787,-0.16212388017938029)
		},
		{
			complex_double(0.53054595023796747,-0.32913427036262427),
			complex_double(0.41491987965405469,0.00916903178669212),
			complex_double(3.99311000236843272,-0.00000000000000041),
			complex_double(-0.53255164920064690,0.40159011368598679)
		},
		{
			complex_double(0.41326818461965009,0.03784292478457726),
			complex_double(0.20582764445535745,0.16212388017937973),
			complex_double(-0.53255164920064535,-0.40159011368598252),
			complex_double(4.57047913898181601,-0.00000000000000007)
		}
	};
	CMatrix mu(z); mu.Print();
	CMatrix htest = mu;
	//Hessenberg(htest); htest.Print();
	CMatrix muH(N), muP(N);
	Hessenberg(mu, muP, muH); muP.Print(); muH.Print();
	complex_double yz[][4] = {
		{
			complex_double(4.6146609698793908,-3.2999999999999999e-16),
			complex_double(0.66522611139219734,0.43167089318211749),
			complex_double(8.9137920819665415e-16,-2.3915274443311944e-15),
			complex_double(-2.0850869609021797e-16,-2.1016216271090411e-15)
		},
		{
			complex_double(0.6652261113921959,-0.43167089318212187),
			complex_double(3.3895718443309182,3.6163563463254e-16),
			complex_double(0.015602265479607483,-0.0010956934968543615),
			complex_double(-9.3544963442049323e-16,3.8584586914414132e-15)
		},
		{
			complex_double(5.7270693802375621e-17,-2.0970150755713915e-17),
			complex_double(0.015602265479609956,0.0010956934968545896),
			complex_double(5.0171976172240909,-4.3351823866832895e-16),
			complex_double(-0.00082160425123789516,9.1366538568338207e-05)
		},
		{
			complex_double(-7.0091059726439015e-17,-6.8033496894478518e-18),
			complex_double(4.3368086899420177e-19,-4.3368086899420177e-19),
			complex_double(-0.00082160425123778413,-9.1366538570225586e-05),
			complex_double(5.0006315320646593,-8.0339380981175879e-16)
		}
	};
	CMatrix Z(yz); Z.Print();
	CVector az(N), bz(N), lambdaz(N);
	CMatrix Qz(N);
	for (int i=0; i<N; ++i)
	{
		//a(i) = O(i,i);
		//b(i) = O(i%(r-1)+1,i%(r-1));
		az(i) = muH(i,i);
		bz(i) = muH(i%(N-1)+1,i%(N-1));
		p(i) = i;
	}
	//az.Print(); bz.Print(); p.Print();
	//TDQREigensolver(N, az.GetPointer(), bz.GetPointer(), lambdaz.GetPointer(), Qz);
	Eig(mu, Qz, lambdaz);

	std::cout << "Q matrix = "; Qz.Print();
	cout << "The eigenvalues are: " << endl;
	lambdaz.Print();
	Y.Print();
	double *vt = Y.GetPointer(0);
	cout << "vt = " << vt[0] << "\n";
	Vector tvt(N, vt); tvt.Print();

	return 0;
}
