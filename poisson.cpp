#include "poisson.h"

const double pi = 3.141592653589793238462643383279502884197169399375105820974\
9445923078164062862089986280348253421170679;

Operator::Operator(const Index &_idx): idx(_idx)
{
	m = idx.G2.Size();
	s = idx.s;
	x = idx.S(0); y = idx.S(1); z = idx.S(2);
	det = idx.R.Det();
}

complex_double *Operator::O(complex_double *in)
{
	complex_double *out = new complex_double[s];
	for (int i=0; i<s; ++i) out[i] = in[i]*det;
	delete[] in;
	return out;
}

complex_double **Operator::O(complex_double **in, const int &ns)
{
	complex_double **out = new complex_double*[s];
	for (int i=0; i<s; ++i) out[i] = new complex_double[ns];
	for (int i=0; i<s; ++i)
		for (int j=0; j<ns; ++j) out[i][j] = in[i][j]*det;
	//for (int i=0; i<s; ++i) delete[] in[i]; delete[] in;
	return out;
}

complex_double *Operator::L(complex_double *in)
{
	double coeff = -1.0*det;
	complex_double *out = new complex_double[m];
	for (int i=0; i<m; ++i) out[i] = in[i]*coeff*idx.G2(i);
	delete[] in;
	return out;
}

complex_double **Operator::L(complex_double **in, const int &ns)
{
	double coeff = -1.0*det;
	complex_double **out = new complex_double*[m];
	for (int i=0; i<m; ++i) out[i] = new complex_double[ns];
	for (int i=0; i<m; ++i)
		for (int j=0; j<ns; ++j) out[i][j] = in[i][j]*coeff*idx.G2(i);
	//for (int i=0; i<m; ++i) delete[] in[i]; delete[] in;
	return out;
}

complex_double *Operator::Linv(complex_double *in)
{
	double coeff = -1.0/det;
	complex_double *out = new complex_double[m];
	out[0] = complex_double(0.0,0.0);
	for (int i=1; i<m; ++i) out[i] = in[i]*coeff/idx.G2(i);
	delete[] in;
	return out;
}

complex_double **Operator::Linv(complex_double **in, const int &ns)
{
	double coeff = -1.0/det;
	complex_double **out = new complex_double*[m];
	for (int i=0; i<m; ++i) out[i] = new complex_double[ns];
	for (int j=0; j<ns; ++j) out[0][j] = complex_double(0.0,0.0);
	for (int i=1; i<m; ++i)
		for (int j=0; j<ns; ++j) out[i][j] = in[i][j]*coeff/idx.G2(i);
	//for (int i=0; i<m; ++i) delete[] in[i]; delete[] in;
	return out;
}

complex_double *Operator::fft3(complex_double *in, const int &f)
{
	complex_double *out = new complex_double[s];
	fftw_complex *fft_in = (fftw_complex*)
		fftw_malloc(sizeof(fftw_complex)*s);
	fftw_complex *fft_out = (fftw_complex*)
		fftw_malloc(sizeof(fftw_complex)*s);
	for (int i=0; i<s; ++i){
		fft_in[i][0] = in[i].real();
		fft_in[i][1] = in[i].imag();
	}
	fftw_plan transform_plan;
	if (f < 0)
	{
		transform_plan = fftw_plan_dft_3d(z, y, x, 
				fft_in, fft_out, FFTW_BACKWARD, 
				FFTW_ESTIMATE);
		fftw_execute(transform_plan);
	}
	else if (f >= 0)
	{
		transform_plan = fftw_plan_dft_3d(z, y, x, 
				fft_in, fft_out, FFTW_FORWARD, 
				FFTW_ESTIMATE);
		fftw_execute(transform_plan);
	}
	for (int i=0; i<s; ++i)
		out[i] = complex_double(fft_out[i][0], fft_out[i][1]);
	fftw_free(fft_in);
	fftw_free(fft_out);
	fftw_destroy_plan(transform_plan);
	fftw_cleanup();
	return out;
}

complex_double *Operator::cJ(const Vector &n)
{
	complex_double *in = new complex_double[s]; 
	complex_double *out;
	for (int i=0; i<s; ++i) in[i] = complex_double(n(i),0.0);
	out = fft3(in, -1);
	delete[] in;
	for (int i=0; i<s; ++i) out[i] /= s;
	return out;
}

complex_double **Operator::cJ(const Matrix &n, const int &ns)
{
	complex_double *vin = new complex_double[s]; 
	complex_double **out = new complex_double*[s]; 
	for (int i=0; i<s; ++i) out[i] = new complex_double[ns];
	for (int j=0; j<ns; ++j)
	{
		for (int i=0; i<s; ++i) vin[i] = n(i,j);
		complex_double *vout = fft3(vin, -1);
		for (int i=0; i<s; ++i) vout[i] /= s;
		for (int i=0; i<s; ++i) out[i][j] = vout[i];
		delete[] vout;
	}
	delete[] vin;
	return out;
}

complex_double *Operator::cJdag(complex_double *in)
{
	complex_double *out;
	out = fft3(in, 1);
	delete[] in;
	for (int i=0; i<s; ++i) out[i] /= s;
	return out;
}

complex_double **Operator::cJdag(complex_double **in, const int &ns)
{
	complex_double *vin = new complex_double[s]; 
	complex_double **out = new complex_double*[s]; 
	for (int i=0; i<s; ++i) out[i] = new complex_double[ns];
	for (int j=0; j<ns; ++j)
	{
		for (int i=0; i<s; ++i) vin[i] = in[i][j];
		complex_double *vout = fft3(vin, 1);
		for (int i=0; i<s; ++i) vout[i] /= s;
		for (int i=0; i<s; ++i) out[i][j] = vout[i];
		delete[] vout;
	}
	delete[] vin;
	//for (int i=0; i<s; ++i) delete[] in[i]; delete[] in;
	return out;
}

complex_double *Operator::cI(complex_double *in)
{
	complex_double *out;
	out = fft3(in, 1);
	delete[] in;
	return out;
}

complex_double **Operator::cI(complex_double **in, const int &ns)
{
	complex_double *vin = new complex_double[s];
	complex_double **out = new complex_double*[s];
	for (int i=0; i<s; ++i) out[i] = new complex_double[ns];
	for (int j=0; j<ns; ++j)
	{
		for (int i=0; i<s; ++i) vin[i] = in[i][j];
		complex_double *vout = fft3(vin, 1);
		for (int i=0; i<s; ++i) out[i][j] = vout[i];
		delete[] vout;
	}
	delete[] vin;
	//for (int i=0; i<s; ++i) delete[] in[i]; delete[] in;
	return out;
}

complex_double *Operator::cIdag(complex_double *in)
{
	complex_double *out = new complex_double[s];
	out = fft3(in, -1);
	delete[] in;
	return out;
}

complex_double **Operator::cIdag(complex_double **in, const int &ns)
{
	complex_double *vin = new complex_double[s];
	complex_double **out = new complex_double*[s];
	for (int i=0; i<s; ++i) out[i] = new complex_double[ns];
	for (int j=0; j<ns; ++j)
	{
		for (int i=0; i<s; ++i) vin[i] = in[i][j];
		complex_double *vout = fft3(vin, -1);
		for (int i=0; i<s; ++i) out[i][j] = vout[i];
		delete[] vout;
	}
	delete[] vin;
	//for (int i=0; i<s; ++i) delete[] in[i]; delete[] in;
	return out;
}

COperator::COperator(const Index &_idx): idx(_idx), m(_idx.G2.Size()),
	s(_idx.s), x(_idx.S(0)), y(_idx.S(1)), z(_idx.S(2))
{
	det = idx.R.Det();
}

CVector COperator::O(const CVector &in) const
{
	CVector out(s);
	for (int i=0; i<s; ++i) out(i) = in(i)*det;
	return out;
}

CMatrix COperator::O(const CMatrix &in) const
{
	CMatrix out(s, in.Columns());
	for (int i=0; i<s; ++i)
		for (int j=0; j<in.Columns(); ++j) out(i,j) = in(i,j)*det;
	return out;
}

CVector COperator::L(const CVector &in) const
{
	double coeff = -1.0*det;
	CVector out(m);
	for (int i=0; i<m; ++i) out(i) = in(i)*coeff*idx.G2(i);
	return out;
}

CMatrix COperator::L(const CMatrix &in) const
{
	double coeff = -1.0*det;
	CMatrix out(m, in.Columns());
	for (int i=0; i<m; ++i)
		for (int j=0; j<in.Columns(); ++j) out(i,j) = in(i,j)*coeff*idx.G2(i);
	return out;
}

CVector COperator::Linv(const CVector &in) const
{
	double coeff = -1.0/det;
	CVector out(m);
	for (int i=1; i<m; ++i) out(i) = in(i)*coeff/idx.G2(i);
	return out;
}

CMatrix COperator::Linv(const CMatrix &in) const
{
	double coeff = -1.0/det;
	CMatrix out(m, in.Columns());
	for (int i=1; i<m; ++i)
		for (int j=0; j<in.Columns(); ++j) out(i,j) = in(i,j)*coeff/idx.G2(i);
	return out;
}

CVector COperator::fft3(const CVector &in, const int f) const
{
	CVector out(s);
	fftw_complex *fft_in = (fftw_complex*)
		fftw_malloc(sizeof(fftw_complex)*s);
	fftw_complex *fft_out = (fftw_complex*)
		fftw_malloc(sizeof(fftw_complex)*s);
	for (int i=0; i<s; ++i){
		fft_in[i][0] = in(i).real();
		fft_in[i][1] = in(i).imag();
	}
	fftw_plan transform_plan;
	if (f < 0)
	{
		transform_plan = fftw_plan_dft_3d(z, y, x, 
				fft_in, fft_out, FFTW_BACKWARD, 
				FFTW_ESTIMATE);
		fftw_execute(transform_plan);
	}
	else if (f >= 0)
	{
		transform_plan = fftw_plan_dft_3d(z, y, x, 
				fft_in, fft_out, FFTW_FORWARD, 
				FFTW_ESTIMATE);
		fftw_execute(transform_plan);
	}
	for (int i=0; i<s; ++i)
		out(i) = complex_double(fft_out[i][0], fft_out[i][1]);
	fftw_free(fft_in);
	fftw_free(fft_out);
	fftw_destroy_plan(transform_plan);
	fftw_cleanup();
	return out;
}

CVector COperator::cJ(const Vector &n) const
{
	CVector in(n);
	CVector out = fft3(in, -1);
	for (int i=0; i<s; ++i) out(i) /= s;
	return out;
}

CMatrix COperator::cJ(const Matrix &n) const
{
	CMatrix out(s, n.Columns());
	for (int j=0; j<n.Columns(); ++j)
	{
		CVector vin(1, j, n);
		CVector vout = fft3(vin, -1);
		for (int i=0; i<s; ++i) vout(i) /= s;
		out.PutColumn(j, vout);
	}
	return out;
}

CVector COperator::cJdag(const CVector &in) const
{
	CVector out = fft3(in, 1);
	for (int i=0; i<s; ++i) out(i) /= s;
	return out;
}

CMatrix COperator::cJdag(const CMatrix &in) const
{
	CMatrix out(s, in.Columns());
	for (int j=0; j<in.Columns(); ++j)
	{
		CVector vin(1, j, in);
		CVector vout = fft3(vin, 1);
		for (int i=0; i<s; ++i) vout(i) /= s;
		out.PutColumn(j, vout);
	}
	return out;
}

CVector COperator::cI(const CVector &in) const
{
	return fft3(in, 1);
}

CMatrix COperator::cI(const CMatrix &in) const
{
	CMatrix out(s, in.Columns());
	for (int j=0; j<in.Columns(); ++j)
	{
		CVector vin(1, j, in);
		CVector vout = fft3(vin, 1);
		out.PutColumn(j, vout);
	}
	return out;
}

CVector COperator::cIdag(const CVector &in) const
{
	return fft3(in, -1);
}

CMatrix COperator::cIdag(const CMatrix &in) const
{
	CMatrix out(s, in.Columns());
	for (int j=0; j<in.Columns(); ++j)
	{
		CVector vin(1, j, in);
		CVector vout = fft3(vin, -1);
		out.PutColumn(j, vout);
	}
	return out;
}

Poisson::Poisson(const COperator &_op): op(_op), s(_op.s)
{}

Vector Poisson::phi(const Vector &n) const
{
	return op.cI(op.Linv(-4.0*pi*op.O(op.cJ(n)))).Real();
}

Matrix Poisson::phi(const Matrix &n) const
{
	return op.cI(op.Linv(-4.0*pi*op.O(op.cJ(n)))).Real();
}

double Poisson::Unum(const Vector &n) const
{
	return 0.5*zdotc(op.cJ(phi(n)), op.O(op.cJ(n))).real();
}

double Poisson::Unum(const Matrix &n) const
{
	return 0.5*trace(zgemm(complex_double(1.), op.cJ(phi(n)).ConjTranspose(),
				op.O(op.cJ(n)))).real();
}
