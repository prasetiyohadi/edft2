#include "dft.h"

const double pi = 3.141592653589793238462643383279502884197169399375105820974\
9445923078164062862089986280348253421170679;

DftSHM::DftSHM(const COperator &_op, const int &_Ns, const double &_f): op(_op),
	ns(_Ns), s(_op.s), f(complex_double(_f)), V_shm(2*_op.idx.dr.Square())
{}

Vector DftSHM::excVWN(const Vector &n) const
{
	const double X1 = 0.75*std::pow((3.0/(2.0*pi)),
				(2.0/3.0));
	const double A  =  0.0310907;
	const double x0 = -0.10498;
	const double b  = 3.72744;
	const double c  = 12.9352;
	const double Q  = std::sqrt(4.*c-b*b);
	const double X0 = x0*x0+b*x0+c;

	Vector rs(s);
	for (int i=0; i<s; ++i)
		rs(i) = std::pow(((4.*pi/3.)*n(i)),(-1./3.));
	Vector x(s);
	for (int i=0; i<s; ++i) x(i) = std::sqrt(rs(i));
	Vector X(s);
	for (int i=0; i<s; ++i)
		X(i) = x(i)*x(i)+b*x(i)+c;

	Vector out(s);
	for (int i=0; i<s; ++i)
	{
		out(i) = -X1/rs(i);
		out(i) += A*(std::log(x(i)*x(i)/X(i))
				+ 2.*b/Q*std::atan(Q/(2.*x(i)+b))
				- ((b*x0)/X0)*(std::log((x(i)-x0)*(x(i)-x0)/X(i))
					+ 2.*(2.*x0*b)/Q*atan(Q/(2.*x(i)+b))));
	}

	return out;
}

Vector DftSHM::excpVWN(const Vector &n) const
{
	const double X1 = 0.75*std::pow((3.0/(2.0*pi)),
				(2.0/3.0));
	const double A  =  0.0310907;
	const double x0 = -0.10498;
	const double b  = 3.72744;
	const double c  = 12.9352;
	const double Q  = std::sqrt(4.*c-b*b);
	const double X0 = x0*x0+b*x0+c;

	Vector rs(s);
	for (int i=0; i<s; ++i)
		rs(i) = std::pow(((4.*pi/3.)*n(i)),(-1./3.));
	Vector x(s);
	for (int i=0; i<s; ++i) x(i) = std::sqrt(rs(i));
	Vector X(s);
	for (int i=0; i<s; ++i)
		X(i) = x(i)*x(i)+b*x(i)+c;

	Vector dx(s);
	for (int i=0; i<s; ++i) dx(i) = 0.5/x(i);

	Vector out(s);
	for (int i=0; i<s; ++i)
	{
		out(i) = dx(i)*(
				2.*X1/(rs(i)*x(i))
				+ A*(2./x(i)-(2.*x(i)+b)/X(i)
					-4.*b/(Q*Q+(2.*x(i)+b)*(2.*x(i)+b))
					-(b*x0)/X0*(
						2./(x(i)-x0)
						-(2.*x(i)+b)/X(i)
						-4.*(2.*x0+b)/(Q*Q+(2.*x(i)+b)*(2.*x(i)+b))
						)
					)
				);
		out(i) = (-rs(i)/(3.*n(i)))*out(i);
	}

	return out;
}

CVector DftSHM::Vdual() const
{
	return op.cJdag(op.O(op.cJ(V_shm)));
}

double DftSHM::getE(const CMatrix &W) const
{
	CMatrix U = zgemm(complex_double(1.), W.ConjTranspose(), op.O(W));
	Vector n = diagouter(zgemm(f, op.cI(W), inverse(U)),
			op.cI(W)).Real();
	CVector phi = -4.*pi*op.Linv(op.O(op.cJ(n)));
	CMatrix T = zgemm(complex_double(1.), W.ConjTranspose(), 
			zgemm(complex_double(1.), op.L(W), inverse(U)));
	complex_double E = -f*trace(T)/complex_double(2.,0.);
	E += zdotc(s, Vdual(), n);
	E += dot(n,op.cJ(op.O(phi).Real()).Real())/2.;
	E += dot(n,op.cJdag(op.O(op.cJ(excVWN(n)))).Real());
	return E.real();
}

CMatrix DftSHM::getGrad(const CMatrix &W) const
{
	complex_double a = complex_double(1.);
	CMatrix U = zgemm(a, W.ConjTranspose(), op.O(W));
	CMatrix grad = H(W);
	grad = zgemm(-a, zgemm(a, op.O(W), inverse(U)), zgemm(a, W.ConjTranspose(),
				H(W)), a, grad);
	grad = zgemm(f, grad, inverse(U));
	return grad;
}

CMatrix DftSHM::H(const CMatrix &W) const
{
	complex_double a = complex_double(1.);
	CMatrix Hw = zgemm(complex_double(-1./2.), op.L(W), zidm(ns));
	CMatrix U = zgemm(a, W.ConjTranspose(), op.O(W));
	Vector n = diagouter(zgemm(f, op.cI(W), inverse(U)),
			op.cI(W)).Real();
	CVector phi = -4.*pi*op.Linv(op.O(op.cJ(n)));
	CVector Veff = Vdual();
	Veff = Veff+op.cJdag(op.O(phi));
	Veff = Veff+op.cJdag(op.O(op.cJ(excVWN(n))));
	Veff = Veff+diagprod(excpVWN(n), op.cJdag(op.O(op.cJ(n))));
	Hw = zgemm(complex_double(1.), op.cIdag(diagprod(Veff, op.cI(W))),
			zidm(ns), complex_double(1.), Hw);
	return Hw;
}

void DftSHM::fdtest(const CMatrix &W) const
{
	double dE = 0.;
	double E0 = getE(W);
	complex_double z = complex_double(1.0);
	CMatrix g0 = getGrad(W);
	CMatrix dW(s, ns); dW.Randomize();
	std::cout << "initial energy: " << E0 << "\n";
	for (double a=10; a>1e-9; a*=1e-1)
	{
		dE = 2*trace(zgemm(a*z, g0.ConjTranspose(), dW)).real();
		std::cout.precision(17);
		std::cout << "ratio actual change to expected change energy:\t"
			<< std::fixed
			<< (getE(zgemmcpy(a*z, dW, zidm(ns), z, W))-E0)/dE
			<< "\n";
		std::cout << "estimate of the error due to rounding:\t\t"
			<< std::fixed
			<< std::sqrt(s)*doubleEps()/std::fabs(dE)
			<< "\n\n";
	}
}

void DftSHM::getPsi(const CMatrix &W, CMatrix &Psi, Vector &eps)
{
	CMatrix mu = zgemm(complex_double(1.,0.), W.ConjTranspose(), H(W));
	CVector zeps(eps);
	Eig(mu, Psi, zeps);
	eps = zeps.Real();
}

void DftSHM::lm(CMatrix &W, Vector &E, const int N) const
{
	const double alpha = 3.0e-5;
	const double tol = 3.0e-5;
	const int s = W.Rows(), ns = W.Columns();
	double a, E0, E1 = getE(W), linmin;
	int i = 0, n;
	Vector Etmp(N);
	CMatrix d(s, ns);
	Etmp(i) = E1;
	while (std::fabs(E1-E0)>tol)
	{
		if (i < N)
		{
			E0 = E1;
			CMatrix g = getGrad(W);
			if (i > 1) 
			{
				linmin = trace(zgemm(complex_double(1.),
							g.ConjTranspose(),d)).real()
					/std::sqrt(trace(zgemm(complex_double(1.),
									g.ConjTranspose(),g)).real()
							*trace(zgemm(complex_double(1.),
									d.ConjTranspose(), d)).real());
				std::cout << "linmin test = " << linmin << "\n";
			}
			i += 1;
			d = -g;
			CMatrix gt = getGrad(W+alpha*d);
			a = alpha*trace(zgemm(complex_double(1.), g.ConjTranspose(),
						d)).real()
				/trace(zgemm(complex_double(1.), (g-gt).ConjTranspose(),
							d)).real();
			W = W+a*d;
			E1 = getE(W);
			Etmp(i) = E1;
			std::cout << "E(" << i << ") = " << getE(W) << "\n";
		}
		else
		{
			std::cout << "Convergence is not reached";
			break;
		}
	}
	n = i+1;
	E.Resize(n);
	for (int j=0; j<n; ++j) E(j) = Etmp(j);
}

void DftSHM::sd(CMatrix &W, const int t) const
{
	const double alpha = 3.0e-5;
	for (int i=0; i<t; ++i)
	{
		W = zgemm(complex_double(-1.*alpha), getGrad(W),
				zidm(ns), complex_double(1.), W);
		std::cout << "E = " << getE(W) << "\n";
	}
}

void DftSHM::sd(CMatrix &W, Vector &E, const int N) const
{
	const double alpha = 3.0e-5;
	const double tol = 3.0e-5;
	double E0, E1 = getE(W);
	int i = 0, n;
	Vector Etmp(N);
	Etmp(i) = E1;
	while (std::fabs(E1-E0)>tol)
	{
		if (i < N)
		{
			i += 1;
			E0 = E1;
			W = zgemm(complex_double(-1.*alpha), getGrad(W),
					zidm(ns), complex_double(1.), W);
			E1 = getE(W);
			Etmp(i) = E1;
			std::cout << "E(" << i << ") = " << getE(W) << "\n";
		}
		else
		{
			std::cout << "Convergence is not reached";
			break;
		}
	}
	n = i+1;
	E.Resize(n);
	for (int j=0; j<n; ++j) E(j) = Etmp(j);
}

DftH::DftH(const COperator &_op, const Matrix &_X, const int _Z, const int &_Ns,
		const double &_f): op(_op), ns(_Ns), s(_op.s), f(complex_double(_f)),
	G2(_op.idx.G2), X(_X), Z(_Z)
{}

Vector DftH::excVWN(const Vector &n) const
{
	const double X1 = 0.75*std::pow((3.0/(2.0*pi)),
				(2.0/3.0));
	const double A  =  0.0310907;
	const double x0 = -0.10498;
	const double b  = 3.72744;
	const double c  = 12.9352;
	const double Q  = std::sqrt(4.*c-b*b);
	const double X0 = x0*x0+b*x0+c;

	Vector rs(s);
	for (int i=0; i<s; ++i)
		rs(i) = std::pow(((4.*pi/3.)*n(i)),(-1./3.));
	Vector x(s);
	for (int i=0; i<s; ++i) x(i) = std::sqrt(rs(i));
	Vector X(s);
	for (int i=0; i<s; ++i)
		X(i) = x(i)*x(i)+b*x(i)+c;

	Vector out(s);
	for (int i=0; i<s; ++i)
	{
		out(i) = -X1/rs(i);
		out(i) += A*(std::log(x(i)*x(i)/X(i))
				+ 2.*b/Q*std::atan(Q/(2.*x(i)+b))
				- ((b*x0)/X0)*(std::log((x(i)-x0)*(x(i)-x0)/X(i))
					+ 2.*(2.*x0*b)/Q*atan(Q/(2.*x(i)+b))));
	}

	return out;
}

Vector DftH::excpVWN(const Vector &n) const
{
	const double X1 = 0.75*std::pow((3.0/(2.0*pi)),
				(2.0/3.0));
	const double A  =  0.0310907;
	const double x0 = -0.10498;
	const double b  = 3.72744;
	const double c  = 12.9352;
	const double Q  = std::sqrt(4.*c-b*b);
	const double X0 = x0*x0+b*x0+c;

	Vector rs(s);
	for (int i=0; i<s; ++i)
		rs(i) = std::pow(((4.*pi/3.)*n(i)),(-1./3.));
	Vector x(s);
	for (int i=0; i<s; ++i) x(i) = std::sqrt(rs(i));
	Vector X(s);
	for (int i=0; i<s; ++i)
		X(i) = x(i)*x(i)+b*x(i)+c;

	Vector dx(s);
	for (int i=0; i<s; ++i) dx(i) = 0.5/x(i);

	Vector out(s);
	for (int i=0; i<s; ++i)
	{
		out(i) = dx(i)*(
				2.*X1/(rs(i)*x(i))
				+ A*(2./x(i)-(2.*x(i)+b)/X(i)
					-4.*b/(Q*Q+(2.*x(i)+b)*(2.*x(i)+b))
					-(b*x0)/X0*(
						2./(x(i)-x0)
						-(2.*x(i)+b)/X(i)
						-4.*(2.*x0+b)/(Q*Q+(2.*x(i)+b)*(2.*x(i)+b))
						)
					)
				);
		out(i) = (-rs(i)/(3.*n(i)))*out(i);
	}

	return out;
}

CVector DftH::Vdual() const
{
	return op.cJ(diagprod(Vps().Real(), op.idx.structureFactor(X).Real()));
}

CVector DftH::Vps() const
{
	int s = op.idx.G2.Size();
	CVector out(s);
	for (int i=0; i<s; ++i) out(i) = -4.0*pi*Z/G2(i);
	out(0) = 0.0e0;
	return out;
}

double DftH::getE(const CMatrix &W) const
{
	CMatrix U = zgemm(complex_double(1.), W.ConjTranspose(), op.O(W));
	Vector n = diagouter(zgemm(f, op.cI(W), inverse(U)),
			op.cI(W)).Real();
	CVector phi = -4.*pi*op.Linv(op.O(op.cJ(n)));
	CMatrix T = zgemm(complex_double(1.), W.ConjTranspose(), 
			zgemm(complex_double(1.), op.L(W), inverse(U)));
	complex_double E = -f*trace(T)/complex_double(2.,0.);
	E += zdotc(s, Vdual(), n);
	E += dot(n,op.cJ(op.O(phi).Real()).Real())/2.;
	E += dot(n,op.cJdag(op.O(op.cJ(excVWN(n)))).Real());
	return E.real();
}

CMatrix DftH::getGrad(const CMatrix &W) const
{
	complex_double a = complex_double(1.);
	CMatrix U = zgemm(a, W.ConjTranspose(), op.O(W));
	CMatrix grad = H(W);
	grad = zgemm(-a, zgemm(a, op.O(W), inverse(U)), zgemm(a, W.ConjTranspose(),
				H(W)), a, grad);
	grad = zgemm(f, grad, inverse(U));
	return grad;
}

CMatrix DftH::H(const CMatrix &W) const
{
	complex_double a = complex_double(1.);
	CMatrix Hw = zgemm(complex_double(-1./2.), op.L(W), zidm(ns));
	CMatrix U = zgemm(a, W.ConjTranspose(), op.O(W));
	Vector n = diagouter(zgemm(f, op.cI(W), inverse(U)),
			op.cI(W)).Real();
	CVector phi = -4.*pi*op.Linv(op.O(op.cJ(n)));
	CVector Veff = Vdual();
	Veff = Veff+op.cJdag(op.O(phi));
	Veff = Veff+op.cJdag(op.O(op.cJ(excVWN(n))));
	Veff = Veff+diagprod(excpVWN(n), op.cJdag(op.O(op.cJ(n))));
	Hw = zgemm(complex_double(1.), op.cIdag(diagprod(Veff, op.cI(W))),
			zidm(ns), complex_double(1.), Hw);
	return Hw;
}

CMatrix DftH::K(const CMatrix &W) const
{
	CMatrix out(W.Rows(), W.Columns());
	for (int i=1; i<W.Rows(); ++i)
		for (int j=0; j<W.Columns(); ++j)
			out(i,j) = W(i,j)*1.0/(1.0+G2(i));
	return out;
}

void DftH::fdtest(const CMatrix &W) const
{
	double dE = 0.;
	double E0 = getE(W);
	complex_double z = complex_double(1.0);
	CMatrix g0 = getGrad(W);
	CMatrix dW(s, ns); dW.Randomize();
	std::cout << "initial energy: " << E0 << "\n";
	for (double a=10; a>1e-9; a*=1e-1)
	{
		dE = 2*trace(zgemm(a*z, g0.ConjTranspose(), dW)).real();
		std::cout.precision(17);
		std::cout << "ratio actual change to expected change energy:\t"
			<< std::fixed
			<< (getE(zgemmcpy(a*z, dW, zidm(ns), z, W))-E0)/dE
			<< "\n";
		std::cout << "estimate of the error due to rounding:\t\t"
			<< std::fixed
			<< std::sqrt(s)*doubleEps()/std::fabs(dE)
			<< "\n\n";
	}
}

void DftH::getPsi(const CMatrix &W, CMatrix &Psi, Vector &eps)
{
	CMatrix mu = zgemm(complex_double(1.,0.), W.ConjTranspose(), H(W));
	CVector zeps(eps);
	Eig(mu, Psi, zeps);
	eps = zeps.Real();
}

void DftH::lm(CMatrix &W, Vector &E, const int N) const
{
	const double alpha = 3.0e-5;
	const double tol = 3.0e-5;
	const int s = W.Rows(), ns = W.Columns();
	double a, E0, E1 = getE(W), linmin;
	int i = 0, n;
	Vector Etmp(N);
	CMatrix d(s, ns);
	Etmp(i) = E1;
	while (std::fabs(E1-E0)>tol)
	{
		if (i < N)
		{
			E0 = E1;
			CMatrix g = getGrad(W);
			if (i > 1) 
			{
				linmin = trace(zgemm(complex_double(1.),
							g.ConjTranspose(),d)).real()
					/std::sqrt(trace(zgemm(complex_double(1.),
									g.ConjTranspose(),g)).real()
							*trace(zgemm(complex_double(1.),
									d.ConjTranspose(), d)).real());
				std::cout << "linmin test = " << linmin << "\n";
			}
			i += 1;
			d = -g;
			CMatrix gt = getGrad(W+alpha*d);
			a = alpha*trace(zgemm(complex_double(1.), g.ConjTranspose(),
						d)).real()
				/trace(zgemm(complex_double(1.), (g-gt).ConjTranspose(),
							d)).real();
			W = W+a*d;
			E1 = getE(W);
			Etmp(i) = E1;
			std::cout << "E(" << i << ") = " << getE(W) << "\n";
		}
		else
		{
			std::cout << "Convergence is not reached";
			break;
		}
	}
	n = i+1;
	E.Resize(n);
	for (int j=0; j<n; ++j) E(j) = Etmp(j);
}

void DftH::pccg(CMatrix &W, Vector &E, const int N, const int cg) const
{
	const double alpha = 3.0e-5;
	const double tol = 3.0e-10;
	const int s = W.Rows(), ns = W.Columns();
	double a, beta = 0.0, cgtest, E0, E1 = getE(W), linmin;
	int i = 0, n;
	Vector Etmp(N);
	CMatrix d(s, ns), g0(s, ns);
	Etmp(i) = E1;
	while (std::fabs(E1-E0)>tol)
	{
		if (i < N)
		{
			E0 = E1;
			CMatrix g = getGrad(W);
			if (i > 1) 
			{
				linmin = trace(zgemm(complex_double(1.),
							g.ConjTranspose(), d)).real()
					/std::sqrt(trace(zgemm(complex_double(1.),
									g.ConjTranspose(), g)).real()
							*trace(zgemm(complex_double(1.),
									d.ConjTranspose(), d)).real());
				cgtest = trace(zgemm(complex_double(1.),
							g.ConjTranspose(), K(g0))).real()
					/std::sqrt(trace(zgemm(complex_double(1.),
									g.ConjTranspose(), K(g))).real()
							*trace(zgemm(complex_double(1.),
									g0.ConjTranspose(), K(g0))).real());
				std::cout << "linmin test = " << linmin << "\n";
				std::cout << "cg test = " << cgtest << "\n";
				switch(cg) {
					case 1:
						beta = trace(zgemm(complex_double(1.0), g.ConjTranspose(),
									K(g))).real()/trace(zgemm(complex_double(1.0),
									g0.ConjTranspose(), K(g0))).real();
						break;
					case 2:
						beta = trace(zgemm(complex_double(1.0),
									(g-g0).ConjTranspose(), K(g))).real()/
							trace(zgemm(complex_double(1.0), g0.ConjTranspose(),
										K(g0))).real();
						break;
					case 3:
						beta = trace(zgemm(complex_double(1.0),
									(g-g0).ConjTranspose(), K(g))).real()/
							trace(zgemm(complex_double(1.0),
										(g-g0).ConjTranspose(), d)).real();
						break;
					default:
						beta = 0.0;
				}
			}
			i += 1;
			d = -K(g+beta*d);
			g0 = g;
			CMatrix gt = getGrad(W+alpha*d);
			a = alpha*trace(zgemm(complex_double(1.), g.ConjTranspose(),
						d)).real()
				/trace(zgemm(complex_double(1.), (g-gt).ConjTranspose(),
							d)).real();
			W = W+a*d;
			E1 = getE(W);
			Etmp(i) = E1;
			std::cout << "E(" << i << ") = " << getE(W) << "\n";
		}
		else
		{
			std::cout << "Convergence is not reached";
			break;
		}
	}
	if (i < N)
	{
		n = i+1;
		E.Resize(n);
		for (int j=0; j<n; ++j) E(j) = Etmp(j);
	}
}

void DftH::pclm(CMatrix &W, Vector &E, const int N) const
{
	const double alpha = 3.0e-5;
	const double tol = 3.0e-5;
	const int s = W.Rows(), ns = W.Columns();
	double a, E0, E1 = getE(W), linmin;
	int i = 0, n;
	Vector Etmp(N);
	CMatrix d(s, ns);
	Etmp(i) = E1;
	while (std::fabs(E1-E0)>tol)
	{
		if (i < N)
		{
			E0 = E1;
			CMatrix g = getGrad(W);
			if (i > 1) 
			{
				linmin = trace(zgemm(complex_double(1.),
							g.ConjTranspose(),d)).real()
					/std::sqrt(trace(zgemm(complex_double(1.),
									g.ConjTranspose(),g)).real()
							*trace(zgemm(complex_double(1.),
									d.ConjTranspose(), d)).real());
				std::cout << "linmin test = " << linmin << "\n";
			}
			i += 1;
			d = -K(g);
			CMatrix gt = getGrad(W+alpha*d);
			a = alpha*trace(zgemm(complex_double(1.), g.ConjTranspose(),
						d)).real()
				/trace(zgemm(complex_double(1.), (g-gt).ConjTranspose(),
							d)).real();
			W = W+a*d;
			E1 = getE(W);
			Etmp(i) = E1;
			std::cout << "E(" << i << ") = " << getE(W) << "\n";
		}
		else
		{
			std::cout << "Convergence is not reached";
			break;
		}
	}
	if (i < N)
	{
		n = i+1;
		E.Resize(n);
		for (int j=0; j<n; ++j) E(j) = Etmp(j);
	}
}

void DftH::sd(CMatrix &W, const int t) const
{
	const double alpha = 3.0e-5;
	for (int i=0; i<t; ++i)
	{
		W = zgemm(complex_double(-1.*alpha), getGrad(W),
				zidm(ns), complex_double(1.), W);
		std::cout << "E = " << getE(W) << "\n";
	}
}

void DftH::sd(CMatrix &W, Vector &E, const int N) const
{
	const double alpha = 3.0e-5;
	const double tol = 3.0e-5;
	double E0, E1 = getE(W);
	int i = 0, n;
	Vector Etmp(N);
	Etmp(i) = E1;
	while (std::fabs(E1-E0)>tol)
	{
		if (i < N)
		{
			i += 1;
			E0 = E1;
			W = zgemm(complex_double(-1.*alpha), getGrad(W),
					zidm(ns), complex_double(1.), W);
			E1 = getE(W);
			Etmp(i) = E1;
			std::cout << "E(" << i << ") = " << getE(W) << "\n";
		}
		else
		{
			std::cout << "Convergence is not reached";
			break;
		}
	}
	n = i+1;
	E.Resize(n);
	for (int j=0; j<n; ++j) E(j) = Etmp(j);
}
