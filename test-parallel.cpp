#include "test-parallel.h"
#include "setup.h"
#include "utils.h"
#include "datatypes.h"

int main(int argc, char *argv[])
{
	const int N = 4;
	const int size = 10;

	int i,j,k,ll,m,isum,ioffset,cnt,N1,N2;
	int mynode, totalnodes;
	MPI_Status status;
	double bn;
	Vector tmpd;
	Matrix tmpdd;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &totalnodes);
	MPI_Comm_rank(MPI_COMM_WORLD, &mynode);


	complex_double z[][4] = 
	{
		{
			complex_double(4.61466096987939078,-0.00000000000000033),
			complex_double(-0.21686561480329894,-0.14072594571898364),
			complex_double(0.53054595023796780,0.32913427036262255),
			complex_double(0.41326818461964887,-0.03784292478458216)
		},
		{
			complex_double(-0.21686561480329866,0.14072594571898545),
			complex_double(4.84381185226941380,-0.00000000000000028),
			complex_double(0.41491987965405497,-0.00916903178669083),
			complex_double(0.20582764445535787,-0.16212388017938029)
		},
		{
			complex_double(0.53054595023796747,-0.32913427036262427),
			complex_double(0.41491987965405469,0.00916903178669212),
			complex_double(3.99311000236843272,-0.00000000000000041),
			complex_double(-0.53255164920064690,0.40159011368598679)
		},
		{
			complex_double(0.41326818461965009,0.03784292478457726),
			complex_double(0.20582764445535745,0.16212388017937973),
			complex_double(-0.53255164920064535,-0.40159011368598252),
			complex_double(4.57047913898181601,-0.00000000000000007)
		}
	};

	CMatrix mu(z);
	CMatrix muH(N), muP(N);
	Hessenberg(mu, muP, muH);

	// Set up storage on each process
	Vector as(size), bs(size), lambdas(size);
	Matrix Qs(size);

	/*for (int i=0; i<size; ++i)
	{
		as(i) = std::abs(muH(i,i));
		bs(i) = std::abs(muH(i%(size-1)+1,i%(size-1)));
	}*/

	for(int i=0;i<size;++i){
		as(i) = 1.5 + i;
		bs(i) = 0.3;
	}

	if (mynode==0)
	{
		clock_t serial0 = clock();
		TDQREigensolverBisec(size, as.GetPointer(), bs.GetPointer(),
				lambdas.GetPointer(), Qs);
		double serial1 = static_cast<double>(clock() - serial0) / CLOCKS_PER_SEC;

		cout << "Serial TDQREigensolver : " << serial1 << "\n";
		cout << "The eigenvalues are: " << endl;
		for (int k=0; k<size ;k++)
		cout << lambdas(k) << endl;
	}

	// Begin parallel code
	Vector a(size), b(size), lambda(size), p(size), d(size), xi(size);
	Matrix Q(size), Q1(size), Q2(size);
	Matrix index(totalnodes);
	Matrix adjust(totalnodes);

	for(int i=0;i<size;++i){
		a(i) = 1.5 + i;
		b(i) = 0.3;
	}

	//Set up recursive partitioning of the matrix.
	//Each process will solve for some subset of the problem

	index(0,0) = size;
	for (i=0; i<log2(totalnodes); ++i)
	{
		isum = 0;
		for (j=0; j<std::pow(2.0,i); ++j)
		{
			index(i+1,2*j) = (int)index(i,j)/2;
			index(i+1,2*j+1) = (int)index(i,j) - (int)index(i+1,2*j);
			isum += (int)index(i+1,2*j);
			adjust(i,j) = b(isum-1);
			a(isum-1) = a(isum-1) - b(isum-1);
			a(isum)   = a(isum)   - b(isum-1);
			isum += (int)index(i+1,2*j+1);
		}
	}

	// Each process solves recursively for its subpart of
	// the problem.
	ioffset = (int) log2(totalnodes);
	isum = 0;
	for (k=0; k<mynode; ++k)
		isum += (int)index(ioffset,k);

    clock_t parallel0 = clock();
	TDQREigensolverBisec((int)index(ioffset,mynode), a.GetPointer(isum),
			b.GetPointer(isum), d.GetPointer(), Q1);

	cout << "totalnodes = " << totalnodes << "\n";
	// Fan-in algorithm to finish solving system
	for (i=0; i<log2(totalnodes); ++i)
	{
		cout << "total i = " << log2(totalnodes) << "\n";
		isum = 0; cnt = 0;
		for (j=0; j<totalnodes; j+=(int)std::pow(2.0,i))
		{
			if (mynode == j)
			{
				cout << "\n\n1st loop mynode = " << j << "\n";
				if (isum%2 == 0)
				{
					cout << "MPIRecv index = " << (int)index(ioffset,isum)
						<< "; total = " << j+(int)std::pow(2.0,i) << "\n";

					MPI_Recv(d.GetPointer((int)index(ioffset,isum)),
							(int)index(ioffset,isum+1),
							MPI_DOUBLE, j+(int)std::pow(2.0,i), 1,
							MPI_COMM_WORLD, &status);
					
					for (k=0; k<(int)index(ioffset,isum+1); ++k)
					{
						cout << "MPIRecv for Q2.Getpointer k, k = " << k
							<< "; index = " << (int)index(ioffset,isum)
							<< "; total = " << j+(int)std::pow(2.0,i) << "\n";

						MPI_Recv(Q2.GetPointer(k), (int)index(ioffset,isum+1),
								MPI_DOUBLE, j+(int)std::pow(2.0,i), 1,
								MPI_COMM_WORLD, &status);
					}
					N1 = (int)index(ioffset,isum);
					N2 = (int)index(ioffset,isum+1);
					bn = adjust(ioffset-1,cnt++);
				}
				else
				{
					cout << "MPISend index = " << (int)index(ioffset,isum)
						<< "; total = " << j-(int)std::pow(2.0,i) << "\n";
					MPI_Send(d.GetPointer(), (int)index(ioffset,isum),
							MPI_DOUBLE, j-(int)std::pow(2.0,i), 1,
							MPI_COMM_WORLD);

					for (k=0; k<(int)index(ioffset,isum); ++k)
					{
						cout << "MPISend for Q1.Getpointer k, k = " << k
							<< "; index = " << (int)index(ioffset,isum)
							<< "; total = " << j-(int)std::pow(2.0,i) << "\n";

						MPI_Send(Q1.GetPointer(k), (int)index(ioffset,isum),
								MPI_DOUBLE, j-(int)pow(2.0,i), 1,
								MPI_COMM_WORLD);
					}
				}
			}
		isum++;
		}
		
		for (j=0; j<totalnodes; j+=(int)std::pow(2.0,i+1))
		{
			if (mynode == j)
			{
				cout << "\n\n2nd loop nmynode = " << j << "\n";
				cnt = 0;
				for (k=0; k<N1; ++k) xi(cnt++) = Q1(N1-1,k);
				for (k=0; k<N2; ++k) xi(cnt++) = Q2(0,k);

				// Solve for the secular equation to
				// obtain eigenvalues
				cout << "solve secular\n";
				SolveSecularEq(N1+N2,bn,d,xi,lambda.GetPointer());

				// Form the Q matrix from Q1 and Q2
				cout << "form Q from Q1 and Q2\n";
				for (k=0; k<N1; ++k)
				{
					for (ll=0; ll<N1+N2; ++ll)
					{
						Q(k,ll) = 0.0;
						for (m=0; m<N1; ++m)
							Q(k,ll) += Q1(k,m)*xi(m)/(d(m)-lambda(ll));
					}
				}
				for (k=0; k<N2; ++k)
				{
					for (ll=0; ll<N1+N2; ++ll)
					{
						Q(N1+k,ll) = 0.0;
						for(m=0; m<N2; ++m)
							Q(k+N1,ll) += Q2(k,m)*xi(N1+m)/(d(N1+m)-lambda(ll));
					}
				}

				// Normalize the Q matrix so that each eigenvector
				// has length one
				cout << "normalize Q\n";
				double sum;
				for (k=0; k<N1+N2; ++k)
				{
					sum = 0.0;
					for (ll=0; ll<N1+N2; ++ll) sum+= Q(ll,k)*Q(ll,k);
					sum = sqrt(sum);
					for (ll=0; ll<N1+N2; ++ll) Q(ll,k) = Q(ll,k)/sum;
				}

				// Swap d and lambda arrays for use in the
				// next part of the fan-in algorithm
				tmpd = d;
				d = lambda;
				lambda = tmpd;

				// Swap Q and Q1 for use in the
				// next part of the fan-in algorithm
				tmpdd = Q1;
				Q1 = Q;
				Q = tmpdd;
			}
		}
		ioffset = ioffset - 1;
	}

	if (mynode==0)
	{
		cout << "\n\nlast mynode = " << j << "\n";
		double parallel1 = static_cast<double>(clock() - parallel0) / CLOCKS_PER_SEC;
		cout << "Parallel TDQREigensolver : " << parallel1 << "\n";
		cout << "The eigenvalues are: " << endl;
		for (k=0; k<size ;k++)
		cout << d(k) << endl;
	}

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	return 0;
}
