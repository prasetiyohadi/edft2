#ifndef _SETUP_H
#define _SETUP_H

#include "edft2.h"
#include "datatypes.h"

struct Index
{
	int dim, s;
	Vector G2, S, dr;
	Matrix G, N, M, R, r;
	Index(const Matrix &_R, const Vector &_S);
	Vector centerDistance() const;
	Vector squareMagnitude(const Matrix &G) const;
	//complex_double *structureFactor(const Matrix &X) const;
	CVector structureFactor(const Matrix &X) const;
	Matrix initM() const;
	Matrix initN() const;
};

struct Density
{
	int Z;
	double sigma1, sigma2, sigma;
	Vector g, g1, g2, n;
	const Index idx;
	Density(const Index &_idx, const int &_Z, const double &_sigma);
	Density(const Index &_idx, const double &_sigma1, const double &_sigma2);
	double Uanal() const;
	double Uself(const Matrix &X) const;
	Vector gaussianDistrib(const Vector &_dr, const double &_sigma) const;
};


#endif
