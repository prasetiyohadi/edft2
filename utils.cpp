#include "utils.h"
#include <random>

double doubleEps()
{
	double eps = 1.0, test = 1.0+eps;
	while(1.0 != test)
	{
		eps = eps/2.0;
		test = 1.0+eps;
	}
	return eps;
}

Matrix set_slice(const Vector& dat, const Vector& S, int l, int m, int *r,
		int *c)
{
	int x, y, z; 
	x = S(0);
	y = S(1);
	z = S(2);
	Tensor T(x, y, z, dat);

	if (m == 0)
	{
		Matrix tmp(y, z);
		for (int k=0; k<z; k++)
			for (int j=0; j<y; j++)
				tmp(j,k) = T(l,j,k);
		*r = y; *c = z;
		return tmp;
		//tmp.Print(); std::cout << "\nbidang YZ\n"; T.PrintYZ();
	}
	if (m == 1)
	{
		Matrix tmp(x, z);
		for (int k=0; k<z; k++)
			for (int i=0; i<x; i++)
				tmp(i,k) = T(i,l,k);
		*r = x; *c = z;
		return tmp;
		//tmp.Print(); std::cout << "\nbidang XZ\n"; T.PrintXZ();
	}
	if (m == 2)
	{
		Matrix tmp(x, y);
		for (int j=0; j<y; j++)
			for (int i=0; i<x; i++)
				tmp(i,j) = T(i,j,l);
		*r = x; *c = y;
		return tmp;
		//tmp.Print(); std::cout << "\nbidang XY\n"; T.PrintXY();
	}
}

Matrix randn(const int &m, const int &n)
{
	// random generator
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::normal_distribution<double> distribution(0.0,1.0);
	Matrix C(n, m);
	for (int i=0; i<n; ++i)
	{
		//distribution.reset();
		for (int j=0; j<m; ++j) C(i,j) = distribution(generator);
	}
	return C.Transpose();
}

void set_dplot_view(const Vector &dat, const Vector &S)
{
	int lim;
	int r, c;
	std::string name = "view-";

	for (int m=0; m<3; m++)
	{
		std::cout << "iteration: " << m+1 << "\n";
		Matrix tmp = set_slice(dat, S, 0, m, &r, &c);
		//out_m(tmp);
		lim = r*c;
		//std::cout << "rows: " << r << ", columns: " << c << ", limit: " <<
		//lim << "\n";
		Matrix res(lim, 3);
		for (int j=0; j<c; j++)
			for (int i=0; i<r; i++)
			{
				res(j*r+i,0) = i*1.0;
				res(j*r+i,1) = j*1.0;
				res(j*r+i,2) = tmp(i,j);
			}
		//out_m(res);
		fout_m(res, name, m, lim, 3);
	}
}

void set_dplot_gaussian(const Vector &dat, const Vector &S)
{
	int lim;
	int r, c;
	std::string name = "gaussian-";

	for (int m=0; m<3; m++)
	{
		std::cout << "iteration: " << m+1 << "\n";
		Matrix tmp = set_slice(dat, S, (int)S(m)/2, m, &r, &c);
		//out_m(tmp);
		lim = r*c;
		//std::cout << "rows: " << r << ", columns: " << c << ", limit: " <<
		//lim << "\n";
		Matrix res(lim, 3);
		for (int j=0; j<c; j++)
			for (int i=0; i<r; i++)
			{
				res(j*r+i,0) = i*1.0;
				res(j*r+i,1) = j*1.0;
				res(j*r+i,2) = tmp(i,j);
			}
		//out_m(res);
		fout_m(res, name, m, lim, 3);
	}
}

void set_dplot_phi(const Vector &dat, const Vector &S)
{
	int lim;
	int r, c;
	std::string name = "phi-";

	for (int m=0; m<3; m++)
	{
		std::cout << "iteration: " << m+1 << "\n";
		Matrix tmp = set_slice(dat, S, (int)S(m)/2, m, &r, &c);
		//out_m(tmp);
		lim = r*c;
		//std::cout << "rows: " << r << ", columns: " << c << ", limit: " <<
		//lim << "\n";
		Matrix res(lim, 3);
		for (int j=0; j<c; j++)
			for (int i=0; i<r; i++)
			{
				res(j*r+i,0) = i*1.0;
				res(j*r+i,1) = j*1.0;
				res(j*r+i,2) = tmp(i,j);
			}
		//out_m(res);
		fout_m(res, name, m, lim, 3);
	}
}

// convert numeric to string
std::string num_to_str(int n)
{
	std::ostringstream stream;
	stream << n;
	return stream.str();
}

std::string num_to_str(double n)
{
	std::ostringstream stream;
	stream << n;
	return stream.str();
}

// print array
void out_a(const Vector &A)
{
    for (int i=0; i<A.Size(); i++) std::cout << A(i) << "\n";
}

// print matrice
void out_m(const Matrix& M)
{
    for (int i=0; i<M.Rows(); i++)
    {
        for (int j=0; j<M.Columns(); j++) std::cout << M(i,j) << "  ";
        std::cout << "\n";
    }
}

// stream array to file
void fout_a(const Vector &A)
{
    std::ofstream fwrite;
    std::string outputFile="output.log";

    fwrite.open(outputFile.c_str());

    for (int i=0; i<A.Size(); i++) fwrite << A(i) << "\n";
    fwrite.close();
}

// stream matrice to file
void fout_m(const Matrix& M, int r, int c)
{
	int rows = M.Rows();
	int columns = M.Columns();

	if ((r>=0) && (c>=0) && (r<=rows) && (c<=columns))
	{
		std::ofstream fwrite;
		std::string outputFile="output.log";
		
		fwrite.open(outputFile.c_str());
		
		for (int i=0; i<r; i++)
		{
			for (int j=0; j<c; j++) fwrite << M(i,j) << "\t";
			fwrite << "\n";
		}
		fwrite.close();
	}
	else std::cerr << "Matrix Error: Invalid Matrix dimension (" << r << ", "
		<< c << "), for Matrix of size " << rows << " x " << columns << "\n";
}

void fout_m(const Matrix& M, std::string name, int r, int c)
{
	int rows = M.Rows();
	int columns = M.Columns();

	if ((r>=0) && (c>=0) && (r<=rows) && (c<=columns))
	{
		std::ofstream fwrite;
		std::string outputFile=name+".out";
		
		fwrite.open(outputFile.c_str());
		
		for (int i=0; i<r; i++)
		{
			for (int j=0; j<c; j++) fwrite << M(i,j) << "\t";
			fwrite << "\n";
		}
		fwrite.close();
	}
	else std::cerr << "Matrix Error: Invalid Matrix dimension (" << r << ", "
		<< c << "), for Matrix of size " << rows << " x " << columns << "\n";
}

void fout_m(const Matrix& M, std::string name, int n, int r, int c)
{
	int rows = M.Rows();
	int columns = M.Columns();

	if ((r>=0) && (c>=0) && (r<=rows) && (c<=columns))
	{
		std::ofstream fwrite;
		std::string outputFile=name+num_to_str(n)+".out";
		
		fwrite.open(outputFile.c_str());
		
		for (int i=0; i<r; i++)
		{
			for (int j=0; j<c; j++) fwrite << M(i,j) << "\t";
			fwrite << "\n";
		}
		fwrite.close();
	}
	else std::cerr << "Matrix Error: Invalid Matrix dimension (" << r << ", "
		<< c << "), for Matrix of size " << rows << " x " << columns << "\n";
}
