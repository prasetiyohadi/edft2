#ifndef _EWALD_H
#define _EWALD_H

#include "edft2.h"
#include "setup.h"
#include "poisson.h"

struct Ewald
{
	int s;
	COperator op;
	Matrix X;
	Ewald(const COperator &_op, const Matrix &_X);
	Vector rn(const Vector &g) const;
	Vector rphi(const Vector &g) const;
	double Unum(const Vector &g) const;
};

#endif
