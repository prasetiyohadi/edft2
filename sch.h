#ifndef _SCH_H
#define _SCH_H

#include "edft2.h"
#include "setup.h"
#include "utils.h"
#include "poisson.h"

struct Sch
{
	int ns, s;
	Vector V_shm;
	//complex_double *Vdual();
	COperator op;
	//Sch(Matrix &R, const Vector &S, const Vector &dr, const int &Ns);
	Sch(const COperator &_op, const int &_Ns);
	double getE(const CMatrix &W) const;
	CVector Vdual() const;
	CMatrix getGrad(const CMatrix &W) const;
	CMatrix H(const CMatrix &W) const;
	void fdtest(const CMatrix &W) const;
	void getPsi(const CMatrix &W, CMatrix &Psi, Vector &eps);
	void sd(CMatrix &W, const int t) const;
	/*double getE(complex_double **W) const;
	complex_double **tes_OW(complex_double **W);
	complex_double** H(complex_double **W);
	complex_double** getGrad(complex_double **W);
	void fdtest(complex_double **W);
	void sd(complex_double **W, const int &t);*/
	/*double getE(Matrix &R, complex_double **W, const Vector &G2,
			const Vector &S);
	complex_double** H(Matrix &R, complex_double **W, const Vector &G2,
			const Vector &S);
	complex_double** getGrad(Matrix &R, complex_double **W, const Vector &G2,
			const Vector &S);
	void fdtest(Matrix &R, complex_double **W, const Vector &G2, const Vector &S);
	void sd(Matrix &R, complex_double **W, const Vector &G2, const Vector &S,
			const int &t);*/
};

#endif
