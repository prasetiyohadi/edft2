#include "datatypes.h"

// Data types definitions

Vector::Vector()
{
	size = 0;
	data = NULL;
}

Vector::Vector(int s)
{
	size = s;
	data = new double[s];
	for (int i=0; i<size; ++i) data[i] = 0.0;
}

Vector::Vector(int s, const double a)
{
	size = s;
	data = new double[s];
	for (int i=0; i<size; ++i) data[i] = a;
}

Vector::Vector(int s, const Vector &v)
{
	size = s;
	data = new double[size];
	if (v.size >= size) for (int i=0; i<size; ++i) data[i] = v.data[i];
	else
	{
		for (int i=0; i<v.size; ++i) data[i] = v.data[i];
		for (int i=v.size; i<size; ++i) data[i] = 0.0;
	}
}

Vector::Vector(const Vector& v)
{
	size = v.size;
	data = new double[size];
	for (int i=0; i<size; ++i)
		data[i] = v.data[i];
}

Vector::Vector(int s, double *q)
{
	size = s;
	data = new double[size];
	for (int i=0; i<size; ++i) data[i] = q[i];
}

Vector::Vector(int f, int d, const Matrix& m)
{
	if (f <= 0)
	{
		size = m.Columns();
		data = new double[size];
		for (int i=0; i<size; ++i) data[i] = m(d,i);
	}
	else if (f > 0)
	{
		size = m.Rows();
		data = new double[size];
		for (int j=0; j<size; ++j) data[j] = m(j,d);
	}
}

Vector::~Vector()
{
	size = 0;
	delete[] data;
	data = NULL;
}

double * Vector::GetPointer()
{
	return data;
}

double * Vector::GetPointer(int s1)
{
	return data+s1;
}

void Vector::Initialize(int s)
{
	if (size != 0) delete[] data;
	size = s;
	data = new double[size];
	for (int i=0; i<size; ++i) data[i] = 0.0;
}

void Vector::Initialize(double a)
{
	for (int i=0; i<size; ++i) data[i] = a;
}

void Vector::Initialize(double *v)
{
	for (int i=0; i<size; ++i) data[i] = v[i];
}

void Vector::Resize(int s)
{
	if (s != size)
	{
		if (data != NULL) delete[] data;
		size = s;
		data = size>0?new double[size]:NULL;
		for (int i=0; i<size; ++i) data[i] = 0.0;
	}
}

Vector Vector::Offset(int offset)
{
	int s = size-offset;
	Vector result(s);
	for (int i=0; i<s; ++i)
		result(i) = data[i+offset];
	return result;
}

int Vector::Size() const
{
	return(size);
}

double Vector::MaxMod() const
{
  double maxm = -1.0e+10;
  for(int i=0; i<size; ++i)
    maxm = (maxm > std::fabs(data[i]))?maxm:std::fabs(data[i]);
  return maxm;
}

double Vector::NormL1() const
{
	double sum = 0.0;
	for (int i=0; i<size; ++i) sum += std::fabs(data[i]);
	return sum;
}

double Vector::NormL2() const
{
	double sum = 0.0;
	for (int i=0; i<size; ++i) sum += data[i]*data[i];
	return std::sqrt(sum);
}

double Vector::NormLinf() const
{
	double max = 0.0, tmp;
	for (int i=0; i<size; ++i)
	{
		tmp = std::fabs(data[i]);
		max = (max>tmp)?max:tmp;
	}
	return max;
}

double Vector::Sum() const
{
	double result = 0.0;
	for (int i=0; i<size; ++i) result += data[i];
	return result;
}

double Vector::Product() const
{
	double result = 1.0;
	for (int i=0; i<size; ++i) result *= data[i];
	return result;
}

double Vector::Length() const
{
	return NormL2();
}

double Vector::SquareLength() const
{
	double sum = 0.0;
	for (int i=0; i<size; ++i) sum += data[i]*data[i];
	return sum;
}

Vector Vector::Square() const
{
	Vector tmp(size);
	for (int i=0; i<size; ++i) tmp(i) = data[i]*data[i];
	return tmp;
}

Vector Vector::Exp() const
{
	Vector tmp(size);
	for (int i=0; i<size; ++i) tmp(i) = std::exp(data[i]);
	return tmp;
}

Vector& Vector::operator=(const Vector& v)
{
	size = v.size;
	for (int i=0; i<size; ++i) data[i] = v.data[i];
	return *this;
}

double Vector::operator()(const int i) const
{
	if (i >= 0 && i < size) return data[i];
	std::cerr << "Vector::Invalid index " << i << " for Vector of size " << 
		size << "\n";
	return 0;
}

double& Vector::operator()(const int i)
{
	if (i >= 0 && i < size) return data[i];
	std::cerr << "Vector::Invalid index " << i << " for Vector of size " << 
		size << "\n";
	return data[0];
}

void Vector::Print() const
{
	std::cout << "\n";
	std::cout << "[ ";
	if (size > 0) std::cout << data[0];
	for (int i=1; i<size; ++i) std::cout << "; " << data[i];
	std::cout << " ]\n";
}

void Vector::Normalize()
{
	double tmp = 1.0/NormL2();
	for (int i=0; i<size; ++i) data[i] = data[i]*tmp;
}

Matrix::Matrix()
{
	rows = 0;
	columns = 0;
	data = NULL;
}

Matrix::Matrix(int s)
{
	rows = s;
	columns = s;
	data = new double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = 0.0;
	}
}

Matrix::Matrix(int r1, int c1)
{
	rows = r1;
	columns = c1;
	data = new double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = 0.0;
	}
}

Matrix::Matrix(int r1, int c1, const double a)
{
	rows = r1;
	columns = c1;
	data = new double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = a;
	}
}

Matrix::Matrix(const Matrix& m)
{
	rows = m.rows;
	columns = m.columns;
	data = new double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = m.data[i][j];
	}
}

Matrix::Matrix(int nv, const Vector* p)
{
	rows = p[0].Size();
	columns = nv;
	data = new double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = p[j](i);
	}
}

Matrix::Matrix(int r1, int c1, double **q)
{
	rows = r1;
	columns = c1;
	data = new double*[rows];
	for (int i=0; i<rows; ++i)
		data[i] = new double[columns];

	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			data[i][j] = q[i][j];
}

Matrix::~Matrix()
{
	for (int i=0; i<rows; ++i)
		delete[] data[i];

	rows = 0;
	columns = 0;
	delete[] data;
}

int Matrix::Rows() const
{
	return rows;
}

int Matrix::Columns() const
{
	return columns;
}

double ** Matrix::GetPointer()
{
	return data;
}

double * Matrix::GetPointer(int r1)
{
	return data[r1];
}

void Matrix::GetColumn(int c, Vector &x)
{
	x.Initialize(0.0);
	for (int i=0; i<rows; ++i) x(i) = data[i][c];
}

void Matrix::GetColumn(int c, Vector &x, int offset)
{
	x.Initialize(0.0);
	for (int i=0; i<rows-offset; ++i) x(i) = data[i+offset][c];
}

void Matrix::PutColumn(int c, const Vector &x)
{
	for (int i=0; i<rows; ++i) data[i][c] = x(i);
}

void Matrix::Resize(int r1, int c1)
{
	if (r1 != rows || c1 != columns)
	{
		if (data != NULL)
		{
			for (int i=0; i<rows; ++i)
				delete[] data[i];
			delete[] data;
		}
		rows = r1;
		columns = c1;
		data = new double*[rows];
		for (int i=0; i<rows; ++i)
		{
			data[i] = new double[columns];
			for (int j=0; j<columns; ++j)
				data[i][j] = 0.0;
		}
	}
}

void Matrix::LUdecomp(Matrix &L, Matrix &U)
{
	L.Initialize(0.0);
	U.Initialize(0.0);
    for (int i = 0; i < rows; ++i)
	{
        // Upper Triangular
        for (int j = i; j < columns; ++j)
		{
            // Summation of L(i, k) * U(k, j)
            double sum = 0.0;
            for (int k = 0; k < i; ++k) sum += L.data[i][k]*U.data[k][j];
			// Evaluating U(i, j)
            U.data[i][j] = data[i][j] - sum;
        }
        // Lower Triangular
        for (int j = i; j < columns; ++j)
		{
			// Assume lower triangular diagonal equals 1.0
            if (i == j) L.data[i][i] = 1.0;
            else
			{
                // Summation of L(j, k) * U(k, i)
                double sum = 0.0;
                for (int k = 0; k < i; ++k) sum += L.data[j][k]*U.data[k][i];
                // Evaluating L(j, i)
                L.data[j][i] = (data[j][i] - sum)/U.data[i][i];
            }
        }
    }
}

void Matrix::LUdecomp(Matrix &lu, Matrix &P, double &pv)
{
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) lu.data[i][j] = data[i][j];
	for (int i=0; i<rows; ++i) P.data[i][i] = 1.0;
	pv = 1.0;
	Vector vs(rows);
	double max, tmp;
	int imax = 0;
	for (int i=0; i<rows; ++i)
	{
		max = 0.0;
		for (int j=0; j<columns; ++j)
			max = (std::fabs(lu.data[i][j])>max)?std::fabs(lu.data[i][j]):max;
		if (max == 0.0) std::cerr << "Matrix Error: singular matrix\n";
		else vs(i) = 1.0/max;
	}
    for (int k=0; k<columns; ++k)
	{
		// Search largest pivot
		max = 0.0;
		for (int i=k; i<rows; ++i)
		{
			tmp = std::abs(lu.data[i][k]);
			if (tmp > max)
			{
				max = tmp;
				imax = i;
			}
		}
		// Swap rows if necessary
		if (k != imax)
		{
			lu.SwapRow(k, imax);
			P.SwapRow(k, imax);
			pv = -pv;
		}
        for (int i=k+1; i<rows; ++i)
		{
			lu.data[i][k] /= lu.data[k][k];
			tmp = lu.data[i][k];
			for (int j=k+1; j<columns; ++j)
				lu.data[i][j] -= tmp*lu.data[k][j];
        }
    }
}

void Matrix::Solve(Vector &x, Vector &b)
{
	x.Initialize(0.0);
	Vector y = x;
	Matrix lu(rows, columns);
	Matrix P(rows, columns);
	double pv;
	LUdecomp(lu, P, pv);
	b = P*b;
    for (int i = 0; i < rows; ++i)
	{
		y(i) = b(i);
		for (int j = 0; j < i; ++j) y(i) -= lu.data[i][j]*y(j);
	}
    for (int i = rows-1; i >= 0; --i)
	{
		x(i) = y(i);
		for (int j = i+1; j < rows; ++j) x(i) -= lu.data[i][j]*x(j);
		x(i) = x(i)/lu.data[i][i];
    }
}

void Matrix::Solve(Matrix &X, Matrix &B)
{
	X.Initialize(0.0);
	Matrix Y = X;
	Matrix lu(rows, columns);
	Matrix P(rows, columns);
	double pv;
	LUdecomp(lu, P, pv);
	B = P*B;
	for (int j=0; j<B.columns; ++j)
	{
		for (int i = 0; i < rows; ++i)
		{
			Y(i,j) = B(i,j);
			for (int k=0; k<i; ++k) Y(i,j) -= lu.data[i][k]*Y(k,j);
		}
		for (int i = rows-1; i >= 0; --i)
		{
			X(i,j) = Y(i,j);
			for (int k=i+1; k<rows; ++k) X(i,j) -= lu.data[i][k]*X(k,j);
			X(i,j) = X(i,j)/lu.data[i][i];
		}
	}
}

Vector Matrix::RowSum() const
{
	Vector v(rows);
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			v(i) += data[i][j];
	return v;
}

Vector Matrix::ColumnSum() const
{
	Vector v(columns);
	for (int j=0; j<columns; ++j)
		for (int i=0; i<rows; ++i)
			v(j) += data[i][j];
	return v;
}

double Matrix::NormLinf()
{
	double max = 0.0, sum;
	for (int i=0; i<rows; ++i)
	{
		sum = 0.0;
		for (int j=0; j<columns; ++j) sum += std::fabs(data[i][j]);
		max = (max>sum)?max:sum;
	}
	return max;
}

double Matrix::NormL1()
{
	double max = 0.0, sum;
	for (int j=0; j<columns; ++j)
	{
		sum = 0.0;
		for (int i=0; i<rows; ++i) sum += std::fabs(data[i][j]);
		max = (max>sum)?max:sum;
	}
	return max;
}

double Matrix::Det()
{
	Matrix lu(rows, columns), P(rows, columns);
	double det;
	LUdecomp(lu, P, det);
	for (int i=0; i<rows; ++i) det *= lu.data[i][i];
	return det;
}

Matrix Matrix::Inverse()
{
	Matrix tmp(rows, columns);
	Matrix I = tmp;
	for (int i=0; i<rows; ++i) I.data[i][i] = 1.0;
	Solve(tmp, I);
	return tmp;
}

Matrix& Matrix::operator=(const Matrix& m)
{
	if ((rows == m.rows) && (columns == m.columns))
		for (int i=0; i<rows; ++i)
			for (int j=0; j<columns; ++j)
				data[i][j] = m.data[i][j];
	else
		std::cerr << "Matrix Error: cannot equate matrices with different \
			sizes\n";
	return *this;
}

double Matrix::operator()(const int i, const int j) const
{
	if ((i>=0) && (j>=0) && (i<rows) && (j<columns)) return data[i][j];
	else std::cerr << "Matrix Error: Invalid Matrix indices (" << i << ", " <<
		j << "), for Matrix of size " << rows << " x " << columns << "\n";
	return (double)0;
}

double& Matrix::operator()(const int i, const int j)
{
	if ((i>=0) && (j>=0) && (i<rows) && (j<columns)) return data[i][j];
	else std::cerr << "Matrix Error: Invalid Matrix indices (" << i << ", " <<
		j << "), for Matrix of size " << rows << " x " << columns << "\n";
	return data[0][0];
}

Matrix Matrix::Transpose() const
{
	Matrix tmp(columns, rows);
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			tmp(j,i) = data[i][j];

	return tmp;
}

Matrix Matrix::Square() const
{
	Matrix tmp(rows, columns);
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			tmp(i,j) = data[i][j]*data[i][j];

	return tmp;
}

void Matrix::SwapRow(int r1, int r2)
{
	double *tmp = data[r1];
	data[r1] = data[r2];
	data[r2] = tmp;
}


void Matrix::Print() const
{
	std::cout << "\n";
	std::cout << "[ ";
	for (int i=0; i<rows; ++i)
	{
		std::cout << data[i][0];
		for (int j=1; j<columns; ++j)
			std::cout << " " << data[i][j];
		if (i!=(rows-1)) std::cout << ";\n  ";
	}
	std::cout << " ]\n";
}

void Matrix::Initialize(double a)
{
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			data[i][j] = a;
}

DiagMatrix::DiagMatrix()
{
	rows = 0;
	columns = 0;
	data = NULL;
}

DiagMatrix::DiagMatrix(int s)
{
	rows = s;
	columns = s;
	data = new double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = 0.0;
	}
}

DiagMatrix::DiagMatrix(int r1, int c1)
{
	rows = r1;
	columns = c1;
	data = new double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = 0.0;
	}
}

DiagMatrix::~DiagMatrix()
{
	for (int i=0; i<rows; ++i)
		delete[] data[i];

	rows = 0;
	columns = 0;
	delete[] data;
}

int DiagMatrix::Rows() const
{
	return rows;
}

int DiagMatrix::Columns() const
{
	return columns;
}

void DiagMatrix::Resize(int r1, int c1)
{
	if (r1 != rows || c1 != columns)
	{
		if (data != NULL)
		{
			for (int i=0; i<rows; ++i)
				delete[] data[i];
			delete[] data;
		}
		rows = r1;
		columns = c1;
		data = new double*[rows];
		for (int i=0; i<rows; ++i)
		{
			data[i] = new double[columns];
			for (int j=0; j<columns; ++j)
				data[i][j] = 0.0;
		}
	}
}

DiagMatrix& DiagMatrix::operator=(const DiagMatrix& m)
{
	if ((rows == m.rows) && (columns == m.columns))
		for (int i=0; i<rows; ++i)
			for (int j=0; j<columns; ++j)
				data[i][j] = m.data[i][j];
	else
		std::cerr << "Matrix Error: cannot equate matrices with different \
			sizes\n";
	return *this;
}

double DiagMatrix::operator()(const int i, const int j) const
{
	if ((i>=0) && (j>=0) && (i<rows) && (j<columns)) return data[i][j];
	else std::cerr << "Matrix Error: Invalid Matrix indices (" << i << ", " <<
		j << "), for Matrix of size " << rows << " x " << columns << "\n";
	return (double)0;
}

double& DiagMatrix::operator()(const int i, const int j)
{
	if ((i>=0) && (j>=0) && (i<rows) && (j<columns)) return data[i][j];
	else std::cerr << "Matrix Error: Invalid Matrix indices (" << i << ", " <<
		j << "), for Matrix of size " << rows << " x " << columns << "\n";
	return data[0][0];
}

void DiagMatrix::Print() const
{
	std::cout << "\n";
	std::cout << "[ ";
	for (int i=0; i<rows; ++i)
	{
		std::cout << data[i][0];
		for (int j=1; j<columns; ++j)
			std::cout << " " << data[i][j];
		if (i!=(rows-1)) std::cout << ";\n  ";
	}
	std::cout << " ]\n";
}

Tensor::Tensor(int s)
{
	x = s;
	y = s;
	z = s;
	data = new double**[z];
	for (int k=0; k<z; ++k)
	{
		data[k] = new double*[y];
		for (int j=0; j<y; ++j)
		{
			data[k][j] = new double[x];
			for (int i=0; i<x; ++i)
				data[k][j][i] = 0.0;
		}
	}
}

Tensor::Tensor(int x1, int y1, int z1)
{
	x = x1;
	y = y1;
	z = z1;
	data = new double**[z];
	for (int k=0; k<z; ++k)
	{
		data[k] = new double*[y];
		for (int j=0; j<y; ++j)
		{
			data[k][j] = new double[x];
			for (int i=0; i<x; ++i)
				data[k][j][i] = 0.0;
		}

	}
}

Tensor::Tensor(const Tensor& t)
{
	x = t.x;
	y = t.y;
	z = t.z;
	data = new double**[z];
	for (int k=0; k<z; ++k)
	{
		data[k] = new double*[y];
		for (int j=0; j<y; ++j)
		{
			data[k][j] = new double[x];
			for (int i=0; i<x; ++i)
				data[k][j][i] = t.data[k][j][i];
		}

	}
}

Tensor::Tensor(int nm, const Matrix * p)
{
	x = p[0].Rows();
	y = p[0].Columns();
	z = nm;
	data = new double**[z];
	for (int k=0; k<z; ++k)
	{
		data[k] = new double*[y];
		for (int j=0; j<y; ++j)
		{
			data[k][j] = new double[x];
			for (int i=0; i<x; ++i)
				data[k][j][i] = p[k](i,j);
		}
	}
}

Tensor::Tensor(int nc, int nm, const Vector ** q)
{
	x = q[0][0].Size();
	y = nc;
	z = nm;
	data = new double**[z];
	for (int k=0; k<z; ++k)
	{
		data[k] = new double*[y];
		for (int j=0; j<y; ++j)
		{
			data[k][j] = new double[x];
			for (int i=0; i<x; ++i)
				data[k][j][i] = q[k][j](i);
		}
	}
}

Tensor::Tensor(int x1, int y1, int z1, const Vector &v)
{
	x = x1;
	y = y1;
	z = z1;
	if (x*y*z == v.Size())
	{
		data = new double**[z];
		for (int k=0; k<z; ++k)
		{
			data[k] = new double*[y];
			for (int j=0; j<y; ++j)
			{
				data[k][j] = new double[x];
				for (int i=0; i<x; ++i)
					data[k][j][i] = v(k*y*x+j*x+i);
			}
		}
	} else
		std::cerr << "Tensor Error: cannot create tensor from vector \
			elements";
}

Tensor::Tensor(int x1, int y1, int z1, double *** r)
{
	x = x1;
	y = y1;
	z = z1;
	data = new double**[z];
	for (int k=0; k<z; ++k)
	{
		data[k] = new double*[x];
		for (int j=0; j<y; y++)
			data[k][j] = r[k][j];
	}
}

Tensor::~Tensor()
{
	for (int k=0; k<z; ++k)
	{
		for (int j=0; j<y; ++j)
			delete[] data[k][j];
		delete[] data[k];
	}

	x = 0;
	y = 0;
	z = 0;
	delete[] data;
}

int Tensor::X() const
{
	return x;
}

int Tensor::Y() const
{
	return y;
}

int Tensor::Z() const
{
	return z;
}

Tensor& Tensor::operator=(const Tensor& t)
{
	if ((x == t.x) && (y == t.y) && (z == t.z))
		for (int k=0; k<z; ++k)
			for (int j=0; j<y; ++j)
				for (int i=0; i<x; ++i)
					data[k][j][i] = t.data[k][j][i];
	else
		std::cerr << "Tensor Error: cannot equate tensor with different \
			sizes\n";
	return *this;
}

double Tensor::operator()(const int i, const int j, const int k) const
{
	if ((i>=0) && (j>=0) && (k>=0) && (i<x) && (j<y) && (k<z))
		return data[k][j][i];
	else std::cerr << "Tensor Error: Invalid Tensor indices (" << i << ", " <<
		j << ", " << k << "), for Tensor of size " << x << " x " << y << " x "
		<< z << "\n";
	return (double)0;
}

double& Tensor::operator()(const int i, const int j, const int k)
{
	if ((i>=0) && (j>=0) && (k>=0) && (i<x) && (j<y) && (k<z))
		return data[k][j][i];
	else std::cerr << "Tensor Error: Invalid Tensor indices (" << i << ", " <<
		j << ", " << k << "), for Tensor of size " << x << " x " << y << " x "
		<< z << "\n";
	return data[0][0][0];
}

void Tensor::Print() const
{
	std::cout << "\n";
	for (int k=0; k<z; ++k)
	{
		std::cout << "[ ";
		for (int j=0; j<y; ++j)
		{
			std::cout << data[k][j][0];
			for (int i=1; i<x; ++i)
				std::cout << " " << data[k][j][i];
			if (j!=(y-1)) std::cout << ";\n  ";
		}
		std::cout << " ]\n\n";
	}
}

void Tensor::PrintXY() const
{
	std::cout << "\n";
	for (int k=0; k<z; ++k)
	{
		std::cout << "[ ";
		for (int j=0; j<y; ++j)
		{
			std::cout << data[k][j][0];
			for (int i=1; i<x; ++i)
				std::cout << " " << data[k][j][i];
			if (j!=(y-1)) std::cout << ";\n  ";
		}
		std::cout << " ]\n\n";
	}
}

void Tensor::PrintXZ() const
{
	std::cout << "\n";
	for (int j=0; j<y; ++j)
	{
		std::cout << "[ ";
		for (int k=0; k<z; ++k)
		{
			std::cout << data[k][j][0];
			for (int i=1; i<x; ++i)
				std::cout << " " << data[k][j][i];
			if (k!=(z-1)) std::cout << ";\n  ";
		}
		std::cout << " ]\n\n";
	}
}

void Tensor::PrintYZ() const
{
	std::cout << "\n";
	for (int i=0; i<x; ++i)
	{
		std::cout << "[ ";
		for (int k=0; k<z; ++k)
		{
			std::cout << data[k][0][i];
			for (int j=1; j<y; ++j)
				std::cout << " " << data[k][j][i];
			if (k!=(z-1)) std::cout << ";\n  ";
		}
		std::cout << " ]\n\n";
	}
}

CVector::CVector()
{
	size = 0;
	data = NULL;
}

CVector::CVector(int s)
{
	size = s;
	data = new complex_double[s];
	for (int i=0; i<size; ++i) data[i] = complex_double(0.,0.);
}

CVector::CVector(int s, const complex_double z)
{
	size = s;
	data = new complex_double[s];
	for (int i=0; i<size; ++i) data[i] = z;
}

CVector::CVector(int s, const double a)
{
	size = s;
	data = new complex_double[s];
	for (int i=0; i<size; ++i) data[i] = complex_double(a);
}

CVector::CVector(int s, const double a, const double b)
{
	size = s;
	data = new complex_double[s];
	for (int i=0; i<size; ++i) data[i] = complex_double(a,b);
}

CVector::CVector(const CVector& v)
{
	size = v.size;
	data = new complex_double[size];
	for (int i=0; i<size; ++i)
		data[i] = v.data[i];
}

CVector::CVector(const Vector& v)
{
	size = v.Size();
	data = new complex_double[size];
	for (int i=0; i<size; ++i)
		data[i] = complex_double(v(i));
}

CVector::CVector(int s, const CVector &v)
{
	size = s;
	data = new complex_double[size];
	if (v.size >= size) for (int i=0; i<size; ++i) data[i] = v.data[i];
	else
	{
		for (int i=0; i<v.size; ++i) data[i] = v.data[i];
		for (int i=v.size; i<size; ++i) data[i] = complex_double(0.,0.);
	}
}

CVector::CVector(int s, complex_double *p)
{
	size = s;
	data = new complex_double[size];
	for (int i=0; i<size; ++i) data[i] = p[i];
}

CVector::CVector(int f, int d, const CMatrix& m)
{
	if (f <= 0)
	{
		size = m.Columns();
		data = new complex_double[size];
		for (int i=0; i<size; ++i) data[i] = m(d,i);
	}
	else if (f > 0)
	{
		size = m.Rows();
		data = new complex_double[size];
		for (int j=0; j<size; ++j) data[j] = m(j,d);
	}
}

CVector::CVector(int f, int d, const Matrix& m)
{
	if (f <= 0)
	{
		size = m.Columns();
		data = new complex_double[size];
		for (int i=0; i<size; ++i) data[i] = complex_double(m(d,i));
	}
	else if (f > 0)
	{
		size = m.Rows();
		data = new complex_double[size];
		for (int j=0; j<size; ++j) data[j] = complex_double(m(j,d));
	}
}

CVector::~CVector()
{
	size = 0;
	delete[] data;
	data = NULL;
}

CVector &CVector::operator=(const CVector &v)
{
	size = v.size;
	for (int i=0; i<size; ++i) data[i] = v.data[i];
	return *this;
}

CVector &CVector::operator=(const Vector &v)
{
	size = v.Size();
	for (int i=0; i<size; ++i) data[i] = complex_double(v(i));
	return *this;
}

complex_double CVector::operator()(const int i) const
{
	if (i >= 0 && i < size) return data[i];
	std::cerr << "CVector::Invalid index " << i << " for CVector of size " << 
		size << "\n";
	return 0;
}

complex_double &CVector::operator()(const int i)
{
	if (i >= 0 && i < size) return data[i];
	std::cerr << "CVector::Invalid index " << i << " for Vector of size " << 
		size << "\n";
	return data[0];
}

int CVector::Size() const
{
	return size;
}

double CVector::MaxMod() const
{
  double maxm = -1.0e+10;
  for(int i=0; i<size; ++i)
    maxm = (maxm > std::abs(data[i]))?maxm:std::abs(data[i]);
  return maxm;
}

double CVector::NormL1() const
{
	double sum = 0.0;
	for (int i=0; i<size; ++i) sum += std::abs(data[i]);
	return sum;
}

double CVector::NormL2() const
{
	double sum = 0.0;
	for (int i=0; i<size; ++i) sum += std::norm(data[i]);
	return std::sqrt(sum);
}

double CVector::NormLinf() const
{
	double max = 0.0, tmp;
	for (int i=0; i<size; ++i)
	{
		tmp = std::abs(data[i]);
		max = (max>tmp)?max:tmp;
	}
	return max;
}

complex_double *CVector::GetPointer()
{
	return data;
}

complex_double *CVector::GetPointer(int s1)
{
	return data+s1;
}

complex_double CVector::IndefNormL2() const
{
	complex_double sum = complex_double(0.,0.);
	for (int i=0; i<size; ++i) sum += data[i]*data[i];
	return std::sqrt(sum);
}

void CVector::Print() const
{
	std::cout << "\n";
	std::cout << "[ ";
	if (size > 0)
		std::cout << data[0].real() << "+(" << data[0].imag() << ")";
	for (int i=1; i<size; ++i)
		std::cout << "; " << data[i].real() << "+(" << data[i].imag() << ")";
	std::cout << " ]\n";
}

CVector CVector::Conjugate() const
{
	CVector result(size);
	for (int i=0; i<size; ++i) result(i) = std::conj(data[i]);
	return result;
}

Vector CVector::Real() const
{
	Vector result(size);
	for (int i=0; i<size; ++i) result(i) = data[i].real();
	return result;
}

void CVector::Initialize(complex_double &z)
{
	for (int i=0; i<size; ++i) data[i] = z;
}

void CVector::Initialize(double a)
{
	for (int i=0; i<size; ++i) data[i] = complex_double(a);
}

void CVector::Scale(complex_double &z)
{
	for (int i=0; i<size; ++i) data[i] *= z;
}

void CVector::Scale(double a)
{
	for (int i=0; i<size; ++i) data[i] *= complex_double(a);
}

CMatrix::CMatrix()
{
	rows = 0;
	columns = 0;
	data = NULL;
}

CMatrix::CMatrix(int s)
{
	rows = s;
	columns = s;
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new complex_double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = complex_double(0.,0.);
	}
}

CMatrix::CMatrix(int r1, int c1)
{
	rows = r1;
	columns = c1;
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new complex_double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = complex_double(0.,0.);
	}
}

CMatrix::CMatrix(int r1, int c1, const complex_double z)
{
	rows = r1;
	columns = c1;
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new complex_double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = z;
	}
}

CMatrix::CMatrix(int r1, int c1, const double a)
{
	rows = r1;
	columns = c1;
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new complex_double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = complex_double(a);
	}
}

CMatrix::CMatrix(int r1, int c1, const double a, const double b)
{
	rows = r1;
	columns = c1;
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new complex_double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = complex_double(a,b);
	}
}

CMatrix::CMatrix(const CMatrix& m)
{
	rows = m.rows;
	columns = m.columns;
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new complex_double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = m.data[i][j];
	}
}

CMatrix::CMatrix(const Matrix& m)
{
	rows = m.Rows();
	columns = m.Columns();
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new complex_double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = complex_double(m(i,j));
	}
}

CMatrix::CMatrix(int c, const CVector *p)
{
	rows = p[0].Size();
	columns = c;
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
	{
		data[i] = new complex_double[columns];
		for (int j=0; j<columns; ++j)
			data[i][j] = p[j](i);
	}
}

CMatrix::CMatrix(int r1, int c1, complex_double **q)
{
	rows = r1;
	columns = c1;
	data = new complex_double*[rows];
	for (int i=0; i<rows; ++i)
		data[i] = new complex_double[columns];

	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			data[i][j] = q[i][j];
}

CMatrix::~CMatrix()
{
	for (int i=0; i<rows; ++i)
		delete[] data[i];

	rows = 0;
	columns = 0;
	delete[] data;
}

CMatrix &CMatrix::operator=(const CMatrix &m)
{
	if ((rows == m.rows) && (columns == m.columns))
		for (int i=0; i<rows; ++i)
			for (int j=0; j<columns; ++j)
				data[i][j] = m.data[i][j];
	else
		std::cerr << "CMatrix Error: cannot equate matrices with different \
			sizes\n";
	return *this;
}

CMatrix &CMatrix::operator=(const Matrix &m)
{
	if ((rows == m.Rows()) && (columns == m.Columns()))
		for (int i=0; i<rows; ++i)
			for (int j=0; j<columns; ++j)
				data[i][j] = complex_double(m(i,j));
	else
		std::cerr << "CMatrix Error: cannot equate matrices with different \
			sizes\n";
	return *this;
}

complex_double CMatrix::operator()(const int i, const int j) const
{
	if ((i>=0) && (j>=0) && (i<rows) && (j<columns)) return data[i][j];
	else std::cerr << "CMatrix Error: Invalid CMatrix indices (" << i << ", "
		<< j << "), for CMatrix of size " << rows << " x " << columns << "\n";
	return (complex_double)0;
}

complex_double &CMatrix::operator()(const int i, const int j)
{
	if ((i>=0) && (j>=0) && (i<rows) && (j<columns)) return data[i][j];
	else std::cerr << "CMatrix Error: Invalid CMatrix indices (" << i << ", " <<
		j << "), for CMatrix of size " << rows << " x " << columns << "\n";
	return data[0][0];
}

complex_double **CMatrix::GetPointer()
{
	return data;
}

int CMatrix::Rows() const
{
	return rows;
}

int CMatrix::Columns() const
{
	return columns;
}

void CMatrix::Print() const
{
	std::cout << "\n";
	std::cout << "[ ";
	for (int i=0; i<rows; ++i)
	{
		std::cout << data[i][0].real() << "+(" << data[i][0].imag() << ")";
		for (int j=1; j<columns; ++j)
			std::cout << " " << data[i][j].real() << "+(" << data[i][j].imag()
				<< ")";
		if (i!=(rows-1)) std::cout << ";\n  ";
	}
	std::cout << " ]\n";
}

CMatrix CMatrix::Conjugate() const
{
	CMatrix result(rows, columns);
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) result(i,j) = std::conj(data[i][j]);
	return result;
}

CMatrix CMatrix::ConjTranspose() const
{
	CMatrix result(columns, rows);
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) result(j,i) = std::conj(data[i][j]);
	return result;
}

CMatrix CMatrix::Transpose() const
{
	CMatrix result(columns, rows);
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) result(j,i) = data[i][j];
	return result;
}

Matrix CMatrix::Real() const
{
	Matrix result(rows, columns);
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) result(i,j) = data[i][j].real();
	return result;
}

void CMatrix::GetColumn(int c, CVector &v)
{
	v.Initialize(0.0);
	for (int i=0; i<rows; ++i) v(i) = data[i][c];
}

void CMatrix::GetColumn(int c, CVector &v, int offset)
{
	v.Initialize(0.0);
	for (int i=0; i<rows-offset; ++i) v(i) = data[i+offset][c];
}

void CMatrix::Initialize(complex_double z)
{
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			data[i][j] = z;
}

void CMatrix::PutColumn(int c, const CVector &v)
{
	for (int i=0; i<rows; ++i) data[i][c] = v(i);
}

void CMatrix::Randomize()
{
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			data[i][j] = complex_double(randn(), randn());
}

void CMatrix::Scale(complex_double &z)
{
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) data[i][j] *= z;
}

void CMatrix::Scale(double a)
{
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) data[i][j] *= complex_double(a);
}

// Structs definition

// Operators definition

Vector operator-(const Vector &v)
{
	Vector result(v.Size());

	for (int i=0; i<v.Size(); ++i)
		result(i) = -v(i);

	return result;
}

Vector operator+(const Vector &v1, const Vector &v2)
{
	int size = minSize(v1,v2);
	Vector result(size);
	
	for (int i=0; i<size; ++i)
		result(i) = v1(i)+v2(i);

	return result;
}

Vector operator-(const Vector &v1, const Vector &v2)
{
	int size = minSize(v1,v2);
	Vector result(size);
	
	for (int i=0; i<size; ++i)
		result(i) = v1(i)-v2(i);

	return result;
}

Vector operator*(const double &a, const Vector &v)
{
	Vector result(v.Size());
	
	for (int i=0; i<v.Size(); ++i)
		result(i) = a*v(i);

	return result;
}

Vector operator*(const Vector &v, const double &b)
{
	Vector result(v.Size());
	
	for (int i=0; i<v.Size(); ++i)
		result(i) = v(i)*b;

	return result;
}

Vector operator*(const Matrix &m, const Vector &v)
{
	int size = m.Rows();
	Vector result(size);

	if (m.Columns() == v.Size())
	{
		for (int i=0; i<size; ++i)
			for (int j=0; j<m.Columns(); ++j)
				result(i) += m(i,j)*v(j);
	}
	else std::cerr << "Vector Error: wrong dimension for matrix-vector \
		multiplication\n";

	return result;
}

Vector operator*(const Vector &v, const Matrix &m)
{
	int size = m.Columns();
	Vector result(size);

	if (m.Rows() == v.Size())
	{
		for (int j=0; j<size; ++j)
			for (int i=0; i<m.Rows(); ++i)
				result(j) += m(i,j)*v(i);
	}
	else std::cerr << "Vector Error: wrong dimension for vector-matrix \
		multiplication\n";

	return result;
}

Vector operator*(const DiagMatrix &m, const Vector &v)
{
	int size = m.Rows();
	Vector result(size);

	if (m.Columns() == v.Size())
	{
		for (int i=0; i<size; ++i) result(i) = m(i,i)*v(i);
	}
	else std::cerr << "Vector Error: wrong dimension for matrix-vector \
		multiplication\n";

	return result;
}

Vector operator*(const Vector &v, const DiagMatrix &m)
{
	int size = m.Columns();
	Vector result(size);

	if (m.Rows() == v.Size())
	{
		for (int j=0; j<size; ++j) result(j) = m(j,j)*v(j);
	}
	else std::cerr << "Vector Error: wrong dimension for vector-matrix \
		multiplication\n";

	return result;
}

Vector operator/(const Vector &v, const double &b)
{
	Vector result(v.Size());

	if (std::fabs(b) > 0)
	{
		for (int i=0; i<v.Size(); ++i)
			result(i) = v(i)/b;
	}
	else std::cerr << "Vector Error: cannot divide vector element by zero\n";

	return result;
}

Matrix operator-(const Matrix &m)
{
	Matrix result(m.Rows(), m.Columns());

	for (int i=0; i<m.Rows(); ++i)
		for (int j=0; j<m.Columns(); ++j)
			result(i,j) = -m(i,j);

	return result;
}

Matrix operator+(const Matrix &m1, const Matrix &m2)
{
	int rows = minRows(m1,m2);
	int columns = minColumns(m1,m2);
	Matrix result(rows, columns);
	
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = m1(i,j)+m2(i,j);

	return result;
}

Matrix operator-(const Matrix &m1, const Matrix &m2)
{
	int rows = minRows(m1,m2);
	int columns = minColumns(m1,m2);
	Matrix result(rows, columns);
	
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = m1(i,j)-m2(i,j);

	return result;
}

Matrix operator*(const Vector &v1, const Vector &v2)
{
	int rows = v1.Size();
	int columns = v2.Size();
	Matrix result(rows, columns);

	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = v1(i)*v2(j);

	return result;
}

Matrix operator*(const double &a, const Matrix &m)
{
	int rows = m.Rows();
	int columns = m.Columns();
	Matrix result(rows, columns);

	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = a*m(i,j);

	return result;
}

Matrix operator*(const Matrix &m, const double &b)
{
	int rows = m.Rows();
	int columns = m.Columns();
	Matrix result(rows, columns);

	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = m(i,j)*b;

	return result;
}

Matrix operator*(const Matrix &A, const Matrix &B)
{
	int rows = A.Rows();
	int columns = B.Columns();
	int iter = A.Columns();
	Matrix result(rows, columns);

	if (A.Columns() == B.Rows())
	{
		for (int i=0; i<rows; ++i)
			for (int j=0; j<columns; ++j)
			{
				result(i,j) = 0.0;
				for (int k=0; k<iter; ++k)
					result(i,j) += A(i,k)*B(k,j);
			}
		return result;
	}
	else std::cerr << "Matrix Error: invalid dimensions for matrix-matrix \
		multiplication\n";
}

Matrix operator*(const DiagMatrix &A, const Matrix &B)
{
	int rows = A.Rows();
	int columns = B.Columns();
	int iter = A.Columns();
	Matrix result(rows, columns);

	if (A.Columns() == B.Rows())
	{
		for (int i=0; i<rows; ++i)
			for (int j=0; j<columns; ++j)
			{
				result(i,j) = 0.0;
				for (int k=0; k<iter; ++k)
					if (i == k) result(i,j) += A(i,k)*B(k,j);
			}
		return result;
	}
	else std::cerr << "Matrix Error: invalid dimensions for matrix-matrix \
		multiplication\n";
}

Matrix operator*(const Matrix &A, const DiagMatrix &B)
{
	int rows = A.Rows();
	int columns = B.Columns();
	int iter = A.Columns();
	Matrix result(rows, columns);

	if (A.Columns() == B.Rows())
	{
		for (int i=0; i<rows; ++i)
			for (int j=0; j<columns; ++j)
			{
				result(i,j) = 0.0;
				for (int k=0; k<iter; ++k)
					if (k == j) result(i,j) += A(i,k)*B(k,j);
			}
		return result;
	}
	else std::cerr << "Matrix Error: invalid dimensions for matrix-matrix \
		multiplication\n";
}

Matrix operator/(const Matrix &m, const double &b)
{
	Matrix result(m.Rows(), m.Columns());

	if (std::fabs(b) > 0)
	{
		for (int i=0; i<m.Rows(); ++i)
			for (int j=0; j<m.Columns(); ++j)
				result(i,j) = m(i,j)/b;
	}
	else std::cerr << "Matrix Error: cannot divide matrix element by zero\n";

	return result;
}

DiagMatrix operator*(const double &a, const DiagMatrix &m)
{
	DiagMatrix result(m.Rows(), m.Columns());

	for (int i=0; i<m.Rows(); ++i) result(i,i) = a*m(i,i);

	return result;
}

DiagMatrix operator*(const DiagMatrix &m, const double &b)
{
	DiagMatrix result(m.Rows(), m.Columns());

	for (int i=0; i<m.Rows(); ++i) result(i,i) = m(i,i)*b;

	return result;
}

DiagMatrix operator/(const DiagMatrix &m, const double &b)
{
	DiagMatrix result(m.Rows(), m.Columns());

	if (std::fabs(b) > 0)
	{
		for (int i=0; i<m.Rows(); ++i) result(i,i) = m(i,i)/b;
	}
	else std::cerr << "Matrix Error: cannot divide matrix element by zero\n";

	return result;
}

CVector operator+(const CVector &v1, const CVector &v2)
{
	int size = minSize(v1,v2);
	CVector result(size);
	
	for (int i=0; i<size; ++i)
		result(i) = v1(i)+v2(i);

	return result;
}

CVector operator-(const CVector &v1, const CVector &v2)
{
	int size = minSize(v1,v2);
	CVector result(size);
	
	for (int i=0; i<size; ++i)
		result(i) = v1(i)-v2(i);

	return result;
}

CVector operator*(const double &a, const CVector &v)
{
	CVector result(v.Size());
	
	for (int i=0; i<v.Size(); ++i)
		result(i) = complex_double(a)*v(i);

	return result;
}

CVector operator*(const CVector &v, const double &b)
{
	CVector result(v.Size());
	
	for (int i=0; i<v.Size(); ++i)
		result(i) = v(i)*complex_double(b);

	return result;
}

CVector operator/(const CVector &v, const double &b)
{
	CVector result(v.Size());

	if (std::fabs(b) > 0)
	{
		for (int i=0; i<v.Size(); ++i)
			result(i) = v(i)/complex_double(b);
	}
	else std::cerr << "CVector Error: cannot divide vector element by zero\n";

	return result;
}

CMatrix operator-(const CMatrix &m)
{
	CMatrix result(m.Rows(), m.Columns());

	for (int i=0; i<m.Rows(); ++i)
		for (int j=0; j<m.Columns(); ++j)
			result(i,j) = -m(i,j);

	return result;
}

CMatrix operator+(const CMatrix &m1, const CMatrix &m2)
{
	int rows = minRows(m1,m2);
	int columns = minColumns(m1,m2);
	CMatrix result(rows, columns);
	
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = m1(i,j)+m2(i,j);

	return result;
}

CMatrix operator-(const CMatrix &m1, const CMatrix &m2)
{
	int rows = minRows(m1,m2);
	int columns = minColumns(m1,m2);
	CMatrix result(rows, columns);
	
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = m1(i,j)-m2(i,j);

	return result;
}

CMatrix operator*(const double &a, const CMatrix &m)
{
	CMatrix result(m.Rows(), m.Columns());

	for (int i=0; i<m.Rows(); ++i)
		for (int j=0; j<m.Columns(); ++j)
			result(i,j) = complex_double(a)*m(i,j);

	return result;
}

CMatrix operator*(const CMatrix &m, const double &b)
{
	CMatrix result(m.Rows(), m.Columns());

	for (int i=0; i<m.Rows(); ++i)
		for (int j=0; j<m.Columns(); ++j)
			result(i,j) = m(i,j)*complex_double(b);

	return result;
}

CMatrix operator/(const CMatrix &m, const double &b)
{
	CMatrix result(m.Rows(), m.Columns());

	if (std::fabs(b) > 0)
	{
		for (int i=0; i<m.Rows(); ++i)
			for (int j=0; j<m.Columns(); ++j)
				result(i,j) = m(i,j)/complex_double(b);
	}
	else std::cerr << "CMatrix Error: cannot divide matrix element by zero\n";

	return result;
}

complex_double* operator*(const DiagMatrix &m, const complex_double *c)
{
	int size = m.Rows();
	complex_double *result = new complex_double[size];

	for (int i=0; i<size; ++i) result[i] = m(i,i)*c[i];

	return result;
	delete[] result;
}

// Methods definitions

int minSize(const Vector &v1, const Vector &v2)
{
	return (v1.Size()>v2.Size())?v1.Size():v2.Size();
}

int minSize(const CVector &v1, const CVector &v2)
{
	return (v1.Size()>v2.Size())?v1.Size():v2.Size();
}

int minRows(const Matrix &m1, const Matrix &m2)
{
	return (m1.Rows()>m2.Rows())?m1.Rows():m2.Rows();
}

int minRows(const CMatrix &m1, const CMatrix &m2)
{
	return (m1.Rows()>m2.Rows())?m1.Rows():m2.Rows();
}

int minColumns(const Matrix &m1, const Matrix &m2)
{
	return (m1.Columns()>m2.Columns())?m1.Columns():m2.Columns();
}

int minColumns(const CMatrix &m1, const CMatrix &m2)
{
	return (m1.Columns()>m2.Columns())?m1.Columns():m2.Columns();
}

double dot(const Vector &v1, const Vector &v2)
{
	int size = minSize(v1,v2);
	double result = 0.0;
	for (int i=0; i<size; ++i)
		result += v1(i)*v2(i);
	return result;
}

double dot(const int n, const Vector &v1, const Vector &v2)
{
	double result = 0.0;
	for (int i=0; i<n; ++i)
		result += v1(i)*v2(i);
	return result;
}

double inner(const Vector &v1, const Vector &v2)
{
	int size = minSize(v1,v2);
	double result = 0.0;

	for (int i=0; i<size; ++i)
		result += v1(i)*v2(i);

	return result;
}

double detDiagMat(const DiagMatrix &m)
{
	double result = 1.0;

	if (m.Rows() == m.Columns())
		for (int i=0; i<m.Rows(); ++i)
			result *= m(i,i);
	else std::cerr << "Matrix Error: matrix is not a square matrix\n";

	return result;
}

double HouseholderTrans(Vector &x, Vector &w){
  double alpha;
  double xm = x.MaxMod();
  for(int i=0; i<x.Size(); ++i) w(i) = x(i)/xm;
  alpha = Sign(w(0))*w.NormL2();
  w(0) = w(0) + alpha;
  alpha = -alpha*xm;
  return alpha;
}

double randn()
{
	// random generator
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::normal_distribution<double> distribution(0.0,1.0);
	return distribution(generator);
	
}

double SecularEq(int N, double bm, double x, Vector &d, Vector &xi)
{
  double sum = 0.0e0;
  for (int i=0; i<N; ++i)
    sum += xi(i)*xi(i)/(d(i)-x);
  sum = bm*sum + 1.0e0;
  return sum;
}

double SecularEq(double bm, int N, double *d, double *xi, double x){
  double sum = 0.0e0;
  for(int i=0;i<N;i++)
    sum += xi[i]*xi[i]/(d[i]-x);
  sum = bm*sum + 1.0e0;
  return sum;
}

double SecularEqImag(int N, complex_double bm, complex_double x, CVector &d,
		CVector &xi)
{
	double alpha, alphaI, beta, betaI, gamma, sum = 0.0e0;
	for (int i=0; i<N; ++i)
	{
		alpha = (xi(i).real()*xi(i).real())-(xi(i).imag()*xi(i).imag());
		beta = 2*xi(i).real()*xi(i).imag();
		gamma = (d(i).real() - x.real())*(d(i).real() - x.real());
		gamma += (d(i).imag() - x.imag())*(d(i).imag() - x.imag());
		alphaI = alpha*bm.imag() + beta*bm.real();
		betaI = alpha*bm.real() - beta*bm.imag();
		sum += (alphaI*(d(i).real() - x.real()) - 
				betaI*(d(i).imag() - x.imag()))/gamma;
	}
	return sum;
}

double SecularEqReal(int N, complex_double bm, complex_double x, CVector &d,
		CVector &xi)
{
	double alpha, alphaR, beta, betaR, gamma, sum = 0.0e0;
	for (int i=0; i<N; ++i)
	{
		alpha = (xi(i).real()*xi(i).real())-(xi(i).imag()*xi(i).imag());
		beta = 2*xi(i).real()*xi(i).imag();
		gamma = (d(i).real() - x.real())*(d(i).real() - x.real());
		gamma += (d(i).imag() - x.imag())*(d(i).imag() - x.imag());
		alphaR = alpha*bm.real() - beta*bm.imag();
		betaR = alpha*bm.imag() + beta*bm.real();
		sum += (alphaR*(d(i).real() - x.real()) + 
				betaR*(d(i).imag() - x.imag()))/gamma;
	}
	sum = sum + 1.0e0;
	return sum;
}

double SecularEqDerivative(int N, double bm, double x, Vector &d, Vector &xi)
{
  double sum = 0.0e0;
  for (int i=0; i<N; ++i)
    sum += xi(i)*xi(i)/((d(i)-x)*(d(i)-x));
  sum = bm*sum;
  return sum;
}

double SecularEqImagDImag(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi)
{
	double alpha, alphaI, beta, betaI, gamma, sum = 0.0e0;
	for (int i=0; i<N; ++i)
	{
		alpha = (xi(i).real()*xi(i).real())-(xi(i).imag()*xi(i).imag());
		beta = 2*xi(i).real()*xi(i).imag();
		gamma = (d(i).real() - x.real())*(d(i).real() - x.real());
		gamma += (d(i).imag() - x.imag())*(d(i).imag() - x.imag());
		alphaI = alpha*bm.imag() + beta*bm.real();
		betaI = alpha*bm.real() - beta*bm.imag();
		sum += (2*(d(i).imag() - x.imag())*(alphaI*(d(i).real() - x.real())-
					betaI*(d(i).imag()-x.imag()))+betaI*gamma)/(gamma*gamma);
	}
	return sum;
}

double SecularEqImagDReal(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi)
{
	double alpha, alphaI, beta, betaI, gamma, sum = 0.0e0;
	for (int i=0; i<N; ++i)
	{
		alpha = (xi(i).real()*xi(i).real())-(xi(i).imag()*xi(i).imag());
		beta = 2*xi(i).real()*xi(i).imag();
		gamma = (d(i).real() - x.real())*(d(i).real() - x.real());
		gamma += (d(i).imag() - x.imag())*(d(i).imag() - x.imag());
		alphaI = alpha*bm.imag() + beta*bm.real();
		betaI = alpha*bm.real() - beta*bm.imag();
		sum += (alphaI*(d(i).real()*d(i).real()-2*d(i).real()*x.real()-
					d(i).imag()*d(i).imag()+2*d(i).imag()*x.imag()+
					x.real()*x.real()-x.imag()*x.imag())-
				2*betaI*(d(i).real()-x.real())*(d(i).imag()-x.imag()))/
			(gamma*gamma);
	}
	return sum;
}

double SecularEqRealDImag(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi)
{
	double alpha, alphaR, beta, betaR, gamma, sum = 0.0e0;
	for (int i=0; i<N; ++i)
	{
		alpha = (xi(i).real()*xi(i).real())-(xi(i).imag()*xi(i).imag());
		beta = 2*xi(i).real()*xi(i).imag();
		gamma = (d(i).real() - x.real())*(d(i).real() - x.real());
		gamma += (d(i).imag() - x.imag())*(d(i).imag() - x.imag());
		alphaR = alpha*bm.real() - beta*bm.imag();
		betaR = alpha*bm.imag() + beta*bm.real();
		sum += (2*alphaR*(d(i).real()-x.real())*(d(i).imag()-x.imag())+
				betaR*(-d(i).real()*d(i).real()+2*d(i).real()*x.real()+
					d(i).imag()*d(i).imag()-2*d(i).imag()*x.imag()-
					x.real()*x.real()+x.imag()*x.imag()))/(gamma*gamma);
	}
	return sum;
}

double SecularEqRealDReal(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi)
{
	double alpha, alphaR, beta, betaR, gamma, sum = 0.0e0;
	for (int i=0; i<N; ++i)
	{
		alpha = (xi(i).real()*xi(i).real())-(xi(i).imag()*xi(i).imag());
		beta = 2*xi(i).real()*xi(i).imag();
		gamma = (d(i).real() - x.real())*(d(i).real() - x.real());
		gamma += (d(i).imag() - x.imag())*(d(i).imag() - x.imag());
		alphaR = alpha*bm.real() - beta*bm.imag();
		betaR = alpha*bm.imag() + beta*bm.real();
		sum += (alphaR*(d(i).real()*d(i).real()-2*d(i).real()*x.real()-
					d(i).imag()*d(i).imag()+2*d(i).imag()*x.imag()+
					x.real()*x.real()-x.imag()*x.imag())-
				2*betaR*(d(i).real()-x.real())*(d(i).imag()-x.imag()))/
			(gamma*gamma);
	}
	return sum;
}

double Sign(const double &x)
{
    double xs;
    xs = (x<0.0)?-1.0:1.0;
    return xs;
}

double **CreateMatrix(int m, int n){
  double ** mat;
  mat = new double*[m];
  for(int i=0;i<m;i++){
    mat[i] = new double[n];
    for(int j=0;j<m;j++)
      mat[i][j] = 0.0;
  }
  return mat;
}

void DestroyMatrix(double ** mat, int m, int n){
  for(int i=0;i<m;i++)
    delete[] mat[i];
  delete[] mat;
}

void Eig(const Matrix &A, Matrix &V, Vector &lambda)
{
	int N = A.Rows();
	Vector a(N), b(N), c(N), p(N);
	Matrix H = A, Vt;
	Hessenberg(H);
	for (int i=0; i<N; ++i)
	{
		a(i) = H(i,i);
		b(i) = H(i%(N-1)+1,i%(N-1));
		p(i) = i;
	}
	TDQREigensolver(N, a.GetPointer(), b.GetPointer(), lambda.GetPointer(), Vt);
	Quicksort(p.GetPointer(), lambda.GetPointer(), 0, N-1);
	for (int i=0; i<N; ++i)
	{
		Vt.GetColumn((int)p(i), c);
		V.PutColumn(i, c);
	}
}

void Eig(const CMatrix &A, CMatrix &V, CVector &lambda)
{
	int N = A.Rows();
	CVector zc(N);
	Vector a(N), b(N), c(N), p(N), lambdar(N);
	CMatrix P(N), H(N);
	Matrix Vr(N);
	Hessenberg(A, P, H); H.Print();
	for (int i=0; i<N; ++i)
	{
		a(i) = std::abs(H(i,i));
		b(i) = std::abs(H(i%(N-1)+1,i%(N-1)));
		p(i) = i;
	}
	TDQREigensolver(N, a.GetPointer(), b.GetPointer(), lambdar.GetPointer(), Vr);
	Quicksort(p.GetPointer(), lambdar.GetPointer(), 0, N-1);
	lambda = lambdar;
	for (int i=0; i<N; ++i)
	{
		Vr.GetColumn((int)p(i), c);
		zc = c;
		V.PutColumn(i, zc);
	}
}

void Hessenberg(Matrix &A)
{
	double beta, gamma;
	int N = A.Rows();
	Vector x(N), w(N);

	for (int k=0; k<N-2; ++k)
	{
		A.GetColumn(k, x, k+1);
		A(k+1,k) = HouseholderTrans(x, w);
		beta = 2.0/dot(N-k-1, w, w);
		for (int i=k+2; i<N; ++i) A(i,k) = w(i-k);
		for (int j=k+1; j<N; ++j)
		{
			gamma = 0.0;
			for (int q=k+1; q<N; ++q) gamma += beta*w(q-k-1)*A(q,j);
			for (int i=k+1; i<N; ++i) A(i,j) = A(i,j) - gamma*w(i-k-1);
		}
		for(int i=0; i<N; ++i)
		{
			gamma = 0.0;
			for (int q=k+1; q<N; ++q) gamma += beta*w(q-k-1)*A(i,q);
			for (int j=k+1; j<N; ++j) A(i,j) = A(i,j) - gamma*w(j-k-1);
		}
	}
}

void Hessenberg(CMatrix &A)
{
	complex_double beta, gamma;
	int N = A.Rows();
	CVector w(N), x(N);
	//complex_double p, q=complex_double(0.,0.);
	//CVector u(N), y(N);

	for (int k=0; k<N-2; ++k)
	{
		A.GetColumn(k, x, k+1);
		std::cout << "x = "; x.Print();
		A(k+1,k) = HouseholderTrans(N-k-1, x, w);
		std::cout << "w = "; w.Print();
		beta = 2.0/zdotu(N-k-1, w, w);
		//p = zdotu(N-k-1, w, w)/2.0; 
		for (int i=k+2; i<N; ++i) A(i,k) = w(i-k);
		for (int j=k+1; j<N; ++j)
		{
			gamma = complex_double(0.,0.);
			for (int q=k+1; q<N; ++q) gamma += beta*w(q-k-1)*A(q,j);
			for (int i=k+1; i<N; ++i) A(i,j) = A(i,j) - gamma*w(i-k-1);
			//for (int q=k+1; q<N; ++q) u(j-k-1) += A(j,q)*w(q-k-1);
			//u(j-k-1) = u(j-k-1)/p;
		}
		/*q = zdotu(N-k-1, w, u)/(2.0*p);
		for (int i=k+1; i<N; ++i) y(i) = u(i-k-1) - q*w(i-k-1);
		for (int i=k+1; i<N; ++i)
		{
			for (int j=k+1; j<N; ++j)
				A(i,j) = A(i,j) - w(j-k-1)*y(i-k-1) - y(j-k-1)*w(i-k-1);
		}*/
		for (int i=0; i<N; ++i)
		{
			gamma = complex_double(0.,0.);
			for (int q=k+1; q<N; ++q) gamma += beta*w(q-k-1)*A(i,q);
			for (int j=k+1; j<N; ++j) A(i,j) = A(i,j) - gamma*w(j-k-1);
		}
	}
}

void Hessenberg(const CMatrix &A, CMatrix &P, CMatrix &H)
// Reduce A to Hessenberg form A = P H P-1 where P is unitary, H is Hessenberg
//                             i.e. P-1 A P = H
// A Hessenberg matrix is upper triangular plus single non-zero diagonal below main diagonal
{
   int N = A.Rows();
   H = A;
   P = zidm(N);

   for (int k=0; k<N-2; k++)             // k is the working column
   {
      // X vector, based on the elements from k+1 down in the kth column
      double xlength = 0;
      for (int i=k+1; i<N; i++) xlength += std::norm(H(i,k));
      xlength = std::sqrt(xlength);

      // U vector ( normalise X - rho.|x|.e_k )
      CVector U(N);
      complex_double rho = 1.0, xk = H(k+1,k);
      double axk = std::abs(xk);
      if (axk > 1.0e-15) rho = -xk/axk;
      U(k+1) = xk - rho * xlength;
      double ulength = std::norm(U(k+1));
      for (int i=k+2; i<N; i++)
      {
         U(i) = H(i,k);
         ulength += std::norm(U(i));
      }
      //ulength = max(sqrt( ulength ), SMALL );
      ulength = (std::sqrt(ulength)>1.0e-30)?std::sqrt(ulength):1.0e-30;
      for (int i=k+1; i<N; i++) U(i)/=ulength;

      // Householder matrix: P = I - 2 U U*T
      CMatrix PK = zidm(N);
      for (int i=k+1; i<N; i++)
      {
         for (int j=k+1; j<N; j++) PK(i,j) -= 2.0*U(i)*std::conj(U(j));
      }

      // Transform as PK*T H PK.   Note: PK is unitary, so PK*T = P
      //H = matMul( PK, matMul( H, PK ) );
	  H = zgemm(complex_double(1.,0.), PK, zgemm(complex_double(1.,0.), H, PK));
      //P = matMul( P, PK );
	  P = zgemm(complex_double(1.,0.), P, PK);
   }
}

void Quicksort(double *p, double *v, int l, int r)
{
	if (l >= r) return;
	double pivot = v[r];
	int cnt = l;
	for (int i=l; i<=r; i++)
		if (v[i] <= pivot)
		{
			Swap(&v[cnt], &v[i]);
			Swap(&p[cnt], &p[i]);
			cnt++;
		}
	Quicksort(p, v, l, cnt-2);
	Quicksort(p, v, cnt, r);
}

void SolveSecularEq(int N, double bm, Vector &d, Vector &xi, double *lambda)
{
	double dyt, x, xt, yt;
	const double offset = 1.0e-5;
	const double tol = 1.0e-6;

	for (int i=0; i<N; ++i)
	{
		xt = d(i) + offset;
		yt = SecularEq(N,bm,xt,d,xi);
		dyt = SecularEqDerivative(N,bm,xt,d,xi);
		x = xt - yt/dyt;
		while (std::fabs(x-xt)>tol)
		{
			xt = x;
			yt = SecularEq(N,bm,xt,d,xi);
			dyt = SecularEqDerivative(N,bm,xt,d,xi);
			x = xt - yt/dyt;
		}
		lambda[i] = x;
	}
}

void SolveSecularEq(int N, complex_double bm, CVector &d, CVector &xi,
		complex_double *lambda)
{
	complex_double dyt, x, xt, yt;
	const double offset = 1.0e-5;
	const double tol = 1.0e-6;
	double denom, u, ur, ui, v, vr, vi, zr, zrt, zi, zit;
	double sign = 1.0e0, signi, signr;
	std::cout << "N = " << N << "\n";

	for (int i=0; i<N; ++i)
	{
		//if (i<N-1) signr = (d(i).real()<d(i+1).real())?sign:-sign;
		//if (i<N-1) signi = (d(i).imag()<d(i+1).imag())?-sign:sign;
		//zrt = d(i).real() + signi*offset;
		//zit = d(i).imag() + signr*offset;
		//xt = complex_double(zrt,zit);
		xt = d(i) + complex_double(offset,offset);
		zrt = xt.real();
		zit = xt.imag();
		u = SecularEqReal(N,bm,xt,d,xi);
		ur = SecularEqRealDReal(N,bm,xt,d,xi);
		ui = SecularEqRealDImag(N,bm,xt,d,xi);
		v = SecularEqImag(N,bm,xt,d,xi);
		vr = SecularEqImagDReal(N,bm,xt,d,xi);
		vi = SecularEqImagDImag(N,bm,xt,d,xi);
		denom = ur*ur+ui*ui;
		std::cout << "denom = " << denom << "\n";
		zr = zrt + (v*ui-u*ur)/denom;
		zi = zit - (v*ur+u*ui)/denom;

		while ((zr-zrt)*(zr-zrt)+(zi-zit)*(zi-zit)>tol*tol)
		{
			zrt = zr;
			zit = zi;
			xt = complex_double(zrt,zit);
			u = SecularEqReal(N,bm,xt,d,xi);
			ur = SecularEqRealDReal(N,bm,xt,d,xi);
			ui = SecularEqRealDImag(N,bm,xt,d,xi);
			v = SecularEqImag(N,bm,xt,d,xi);
			vr = SecularEqImagDReal(N,bm,xt,d,xi);
			vi = SecularEqImagDImag(N,bm,xt,d,xi);
			denom = ur*ur+ui*ui;
			zr = zrt + (v*ui-u*ur)/denom;
			zi = zit - (v*ur+u*ui)/denom;
			std::cout << "denom = " << denom << "\n";
			std::cout << "zr-zrt = " << std::abs(zr-zrt) << "\n";
			std::cout << "zi-zit = " << std::abs(zi-zit) << "\n";
		}
		lambda[i] = complex_double(zr,zi);
		std::cout << "lambda[" << i << "] = " << lambda[i] << "\n";
	}
}

void SolveSecularEqBisec(int N, double bm, Vector &d, Vector &xi,
		double *lambda)
{
	
	double xl,xr,xm;
	double yl,yr,ym;
	const double offset = 1.0e-5;
	const double tol = 1.0e-6;

	for (int i=0; i<N-1; ++i)
	{
		xl = d(i) + offset;
		yl = SecularEq(N,bm,xl,d,xi);
		xr = d(i+1) - offset;
		yr = SecularEq(N,bm,xr,d,xi);
		xm = 0.5*(xl+xr);
		ym = SecularEq(N,bm,xm,d,xi);
	
		if(yl*yr > 0){
			lambda[i] = xl;
			continue;
		}
		
		while (std::fabs(ym)>tol)
		{
			if (yl*ym<0) xr = xm;
			else xl = xm;
			xm = 0.5*(xl+xr);
			ym = SecularEq(N,bm,xm,d,xi);
			if (std::fabs(ym) > 1e100)
				{
					std::cout << "secular divergent, abort"; break;
				}
		}
		lambda[i] = xm;
	}

	xl = d(N-1) + offset;
	yl = SecularEq(N,bm,xl,d,xi);
	xr = 2*d(N-1);
	yr = SecularEq(N,bm,xr,d,xi);
	xm = 0.5*(xl+xr);
	ym = SecularEq(N,bm,xm,d,xi);
	
	if (yl*yr > 0)
	{
		lambda[N-1] = xl;
	}
	else
	{
		while (std::fabs(ym)>tol)
		{
			if (yl*ym<0) xr = xm;
			else xl = xm;
			xm = 0.5*(xl+xr);
			ym = SecularEq(N,bm,xm,d,xi);
			if (std::fabs(ym) > 1e100)
				{
					std::cout << "secular divergent, abort"; break;
				}
		}
		lambda[N-1] = xm;
	}
}

void SolveSecularEq(double bm, int N, double *d, 
		    double *xi, double * lambda){
  
  double xl,xr,xm;
  double yl,yr,ym;
  const double offset = 1.0e-5;
  const double tol = 1.0e-6;


  for(int i=0;i<N-1;i++){
    xl = d[i] + offset;
    yl = SecularEq(bm,N,d,xi,xl);
    xr = d[i+1] - offset;
    yr = SecularEq(bm,N,d,xi,xr);
    xm = 0.5*(xl+xr);
    ym = SecularEq(bm,N,d,xi,xm);
   
    if(yl*yr > 0){
      lambda[i] = xl;
      continue;
    }
    
    while(fabs(ym)>tol){
      if(yl*ym<0)
	xr = xm;
      else
	xl = xm;
      xm = 0.5*(xl+xr);
      ym = SecularEq(bm,N,d,xi,xm);
			if (std::fabs(ym) > 1e100)
			{
				std::cout << "secular divergent, abort"; break;
			}
    }
    lambda[i] = xm;
  }

  xl = d[N-1] + offset;
  yl = SecularEq(bm,N,d,xi,xl);
  xr = 2*d[N-1];
  yr = SecularEq(bm,N,d,xi,xr);
  xm = 0.5*(xl+xr);
  ym = SecularEq(bm,N,d,xi,xm);
  
  if(yl*yr > 0){
    lambda[N-1] = xl;
  }
  else{
    while(fabs(ym)>tol){
      if(yl*ym<0)
	xr = xm;
      else
	xl = xm;
      xm = 0.5*(xl+xr);
      ym = SecularEq(bm,N,d,xi,xm);
			if (std::fabs(ym) > 1e100)
			{
				std::cout << "secular divergent, abort"; break;
			}
    }
    lambda[N-1] = xm;
  }
}

void Swap(double &a, double &b)
{
    double tmp = a;
    a = b;
    b = tmp;
}

void Swap(double *a, double *b)
{
    double tmp = *a;
    *a = *b;
    *b = tmp;
}

void TDQREigensolver(int N, double *a, double *b, double *lambda, Matrix &Q)
{
	if (N == 1)
	{
		Q(0,0) = 1.0;
		lambda[0] = a[0];
	}
	else
	{
		int N1 = N/2;
		int N2 = N-N1;
		Vector d(N);
		Matrix Q1(N1);
		Matrix Q2(N2);
		
		a[N1-1] = a[N1-1] - b[N1-1];
		a[N1]   = a[N1]   - b[N1-1];
		TDQREigensolver(N1, a, b, d.GetPointer(), Q1);
		TDQREigensolver(N2, &a[N1], &b[N1], d.GetPointer(N1), Q2);

		Vector xi(N);
		int cnt = 0;
		for (int i=0; i<N1; ++i) xi(cnt++) = Q1(N1-1,i);
		for (int i=0; i<N2; ++i) xi(cnt++) = Q2(0,i);

		SolveSecularEq(N, b[N1-1], d, xi, lambda);

		for (int i=0; i<N1; ++i)
		{
			for (int j=0; j<N; ++j)
			{
				Q(i,j) = 0.0;
				for (int k=0; k<N1; ++k)
					Q(i,j) += Q1(i,k)*xi(k)/(d(k)-lambda[j]);
			}
		}

		for (int i=0; i<N2; ++i)
		{
			for (int j=0; j<N; ++j)
			{
				Q(N1+i,j) = 0.0;
				for (int k=0; k<N2; ++k)
					Q(N1+i,j) += Q2(i,k)*xi(N1+k)/(d(N1+k)-lambda[j]);
			}
		}

		double sum;
		for (int i=0; i<N; ++i)
		{
			sum = 0.0;
			for (int j=0; j<N; ++j)
				sum += Q(j,i)*Q(j,i);
			sum = std::sqrt(sum);
			for (int j=0; j<N; ++j) Q(j,i) = Q(j,i)/sum;
		}
	}
}

void TDQREigensolver(int N, complex_double *a, complex_double *b,
		complex_double *lambda, CMatrix &Q)
{
	if (N == 1)
	{
		Q(0,0) = complex_double(1.0);
		lambda[0] = a[0];
	}
	else
	{
		int N1 = N/2;
		int N2 = N-N1;
		CVector d(N);
		CMatrix Q1(N1);
		CMatrix Q2(N2);
		
		a[N1-1] = a[N1-1] - b[N1-1];
		a[N1]   = a[N1]   - b[N1-1];
		TDQREigensolver(N1, a, b, d.GetPointer(), Q1);
		TDQREigensolver(N2, &a[N1], &b[N1], d.GetPointer(N1), Q2);

		CVector xi(N);
		int cnt = 0;
		for (int i=0; i<N1; ++i) xi(cnt++) = Q1(N1-1,i);
		for (int i=0; i<N2; ++i) xi(cnt++) = Q2(0,i);

		SolveSecularEq(N, b[N1-1], d, xi, lambda);

		for (int i=0; i<N1; ++i)
		{
			for (int j=0; j<N; ++j)
			{
				Q(i,j) = complex_double(0.0);
				for (int k=0; k<N1; ++k)
					Q(i,j) += Q1(i,k)*xi(k)/(d(k)-lambda[j]);
			}
		}

		for (int i=0; i<N2; ++i)
		{
			for (int j=0; j<N; ++j)
			{
				Q(N1+i,j) = complex_double(0.0);
				for (int k=0; k<N2; ++k)
					Q(N1+i,j) += Q2(i,k)*xi(N1+k)/(d(N1+k)-lambda[j]);
			}
		}

		complex_double sum;
		for (int i=0; i<N; ++i)
		{
			sum = complex_double(0.0);
			for (int j=0; j<N; ++j)
				sum += Q(j,i)*Q(j,i);
			sum = std::sqrt(sum);
			for (int j=0; j<N; ++j) Q(j,i) = Q(j,i)/sum;
		}
	}
}

void TDQREigensolver(int N, double *a, double *b, 
		     double *lambda, double **Q){

  if(N == 1){
    Q[0][0] = 1.0;
    lambda[0] = a[0];
  }
  else{
    int i,j,k,N1,N2,cnt;
    N1 = N/2;
    N2 = N-N1;
    double * d = new double[N]; 
    double ** Q1 = CreateMatrix(N1,N1);
    double ** Q2 = CreateMatrix(N2,N2);
    
    a[N1-1] = a[N1-1] - b[N1-1];
    a[N1]   = a[N1]   - b[N1-1];
    TDQREigensolver(N1,a,b,d,Q1);
    TDQREigensolver(N2,&a[N1],&b[N1],&d[N1],Q2);

    double * xi = new double[N];
    cnt = 0;
    for(i=0;i<N1;i++)
      xi[cnt++] = Q1[N1-1][i];
    for(i=0;i<N2;i++)
      xi[cnt++] = Q2[0][i];

    SolveSecularEq(b[N1-1],N,d,xi,lambda);
    
    for(i=0;i<N1;i++){
      for(j=0;j<N;j++){
	Q[i][j] = 0.0;
	for(k=0;k<N1;k++)
	  Q[i][j] += Q1[i][k]*xi[k]/(d[k]-lambda[j]);
      }
    }
    for(i=0;i<N2;i++){
      for(j=0;j<N;j++){
	Q[N1+i][j] = 0.0;
	for(k=0;k<N2;k++)
	  Q[i+N1][j] += Q2[i][k]*xi[N1+k]/(d[N1+k]-lambda[j]);
      }
    }

    double sum;
    for(i=0;i<N;i++){
      sum = 0.0;
      for(j=0;j<N;j++)
	sum+= Q[j][i]*Q[j][i];
      sum = sqrt(sum);
      for(j=0;j<N;j++)
	Q[j][i] = Q[j][i]/sum;
    }
    
    delete[] xi;
    delete[] d;
    DestroyMatrix(Q1,N1,N1);
    DestroyMatrix(Q2,N2,N2);
  }
  return;
}

void TDQREigensolverBisec(int N, double *a, double *b, double *lambda,
		Matrix &Q)
{
	if (N == 1)
	{
		Q(0,0) = 1.0;
		lambda[0] = a[0];
	}
	else
	{
		int N1 = N/2;
		int N2 = N-N1;
		Vector d(N);
		Matrix Q1(N1);
		Matrix Q2(N2);
		
		a[N1-1] = a[N1-1] - b[N1-1];
		a[N1]   = a[N1]   - b[N1-1];
		TDQREigensolver(N1, a, b, d.GetPointer(), Q1);
		TDQREigensolver(N2, &a[N1], &b[N1], d.GetPointer(N1), Q2);

		Vector xi(N);
		int cnt = 0;
		for (int i=0; i<N1; ++i) xi(cnt++) = Q1(N1-1,i);
		for (int i=0; i<N2; ++i) xi(cnt++) = Q2(0,i);

		SolveSecularEqBisec(N, b[N1-1], d, xi, lambda);

		for (int i=0; i<N1; ++i)
		{
			for (int j=0; j<N; ++j)
			{
				Q(i,j) = 0.0;
				for (int k=0; k<N1; ++k)
					Q(i,j) += Q1(i,k)*xi(k)/(d(k)-lambda[j]);
			}
		}

		for (int i=0; i<N2; ++i)
		{
			for (int j=0; j<N; ++j)
			{
				Q(N1+i,j) = 0.0;
				for (int k=0; k<N2; ++k)
					Q(N1+i,j) += Q2(i,k)*xi(N1+k)/(d(N1+k)-lambda[j]);
			}
		}

		double sum;
		for (int i=0; i<N; ++i)
		{
			sum = 0.0;
			for (int j=0; j<N; ++j)
				sum += Q(j,i)*Q(j,i);
			sum = std::sqrt(sum);
			for (int j=0; j<N; ++j) Q(j,i) = Q(j,i)/sum;
		}
	}
}

Vector diagprod(const Vector &v1, const Vector &v2)
{
	Vector v(v2.Size());
	if (v1.Size() == v2.Size())
		for (int i=0; i<v2.Size(); ++i)
			v(i) = v1(i)*v2(i);
	return v;
}

Vector diagVec(const Matrix &m)
{
	int size = m.Rows();
	Vector result(size);

	if (m.Rows() == m.Columns())
	{
		for (int i=0; i<size; ++i)
			result(i) = m(i,i);

	}
	else std::cerr << "Matrix Error: matrix is not square matrix\n";
	
	return result;
}

Vector diagVec(const DiagMatrix &m)
{
	int size = m.Rows();
	Vector result(size);

	if (m.Rows() == m.Columns())
	{
		for (int i=0; i<size; ++i)
			result(i) = m(i,i);

	}
	else std::cerr << "Matrix Error: matrix is not square matrix\n";
	
	return result;
}

Matrix curl(const Vector &v1, const Vector &v2)
{
	int rows = v1.Size();
	int columns = v2.Size();
	Matrix result(rows, columns);

	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = v1(i)*v2(j);

	return result;
}

DiagMatrix diagMat(const Vector &v)
{
	int s = v.Size();
	DiagMatrix result(s);
	for (int i=0; i<s; ++i) result(i,i) = v(i);
	return result;
}

DiagMatrix invDiagMat(const Vector &v)
{
	int s = v.Size();
	DiagMatrix result(s);
	for (int i=0; i<s; ++i) result(i,i) = 1.0/v(i);
	return result;
}

DiagMatrix invDiagMat(const DiagMatrix &m)
{
	int s = m.Rows();
	DiagMatrix result(s);
	for (int i=0; i<s; ++i) result(i,i) = 1.0/m(i,i);
	return result;
}

Matrix outer(const Vector &v1, const Vector &v2)
{
	int rows = v1.Size();
	int columns = v2.Size();
	Matrix result(rows, columns);

	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j)
			result(i,j) = v1(i)*v2(j);

	return result;
}

complex_double HouseholderTrans(int s, CVector &x, CVector &w){
  complex_double xm = x.MaxMod();
  for (int i=0; i<s; ++i) w(i) = x(i)/xm;
  complex_double alpha = zdotu(s, w, w);
  alpha = Sign(w(0))*std::sqrt(alpha);
  w(0) = w(0) + alpha/xm;
  return -alpha;
}

complex_double Sign(const complex_double &z)
{
    complex_double zs;
	zs = complex_double(z.real()/std::abs(z), z.imag()/std::abs(z));
    return zs;
}

complex_double sum(const int &n, complex_double *x)
{
	complex_double y = complex_double(0.0,0.0);
	for (int i=0; i<n; ++i) y = y+x[i];
	return y;
	delete[] x;
}

complex_double trace(const CMatrix &A)
{
	complex_double result(0.0,0.0);
	if (A.Rows() == A.Columns())
		for (int i=0; i<A.Rows(); ++i) result += A(i,i);
	return result;
}

complex_double trace(const int &m, const int &n, complex_double **A)
{
	complex_double result(0.0,0.0);
	if (m == n)
		for (int i=0; i<m; ++i) result += A[i][i];
	return result;
	for (int i=0; i<m; ++i) delete[] A; delete[] A;
}

complex_double trace(const int m, const int n, const CMatrix &A)
{
	complex_double result(0.0,0.0);
	if (m == n)
		for (int i=0; i<m; ++i) result += A(i,i);
	return result;
}

complex_double zdotc(const int &s, complex_double *x, complex_double *y)
{
	complex_double z = complex_double(0.0,0.0);
	for (int i=0; i<s; ++i) z = z+std::conj(x[i])*y[i];
	return z;
	delete[] x;
	delete[] y;
}

complex_double zdotc(const CVector &x, const CVector &y)
{
	complex_double z = complex_double(0.,0.);
	if (x.Size() == y.Size())
		for (int i=0; i<x.Size(); ++i) z = z+std::conj(x(i))*y(i);
	return z;
}

complex_double zdotc(const int s, const CVector &x, const CVector &y)
{
	complex_double z = complex_double(0.,0.);
	if ((s <= x.Size()) && (s <= y.Size()))
		for (int i=0; i<s; ++i) z = z+std::conj(x(i))*y(i);
	return z;
}

complex_double zdotu(const int s, CVector &x, CVector &y)
{
	complex_double z = complex_double(0.,0.);
	if ((s <= x.Size()) && (s <= y.Size()))
		for (int i=0; i<s; ++i) z = z+x(i)*y(i);
	return z;
}

CVector diagouter(const CMatrix &A, const CMatrix &B)
{
	CVector result(A.Rows());
	if ((A.Rows() == B.Rows()) && (A.Columns() == B.Columns()))
	{
		int r = A.Rows();
		int c = B.Columns();
		for (int i=0; i<r; ++i)
			for (int j=0; j<c; ++j)
				result(i) += A(i,j)*std::conj(B(i,j));
	}
	return result;
}

CVector diagprod(const Vector &x, const CVector &z)
{
	CVector v(z.Size());
	if (x.Size() == z.Size())
		for (int i=0; i<z.Size(); ++i)
			v(i) = x(i)*z(i);
	return v;
}

CVector diagprod(const CVector &v1, const CVector &v2)
{
	CVector v(v2.Size());
	if (v1.Size() == v2.Size())
		for (int i=0; i<v2.Size(); ++i)
			v(i) = v1(i)*v2(i);
	return v;
}

CVector zgbmv(const int m, const int n, const complex_double alpha,
		const CMatrix &A, const CVector &x)
{
	CVector y(n);
	if (m == n)
		for (int i=0; i<m; ++i)
			y(i) = alpha*A(i,i)*x(i);
	return y;
}

CVector zgbvv(const int m, const int n, const complex_double alpha,
		const CVector &x, const CVector &y)
{
	CVector result(m);
	if (m == n)
		for (int i=0; i<m; ++i)
			result(i) = alpha*x(i)*y(i);
	return result;
}

CMatrix ccholesky(const CMatrix &A)
{
	if (A.Rows() == A.Columns())
	{
		int s = A.Rows();
		CMatrix L(s);
		for (int i=0; i<s; ++i)
			for (int j=0; j<(i+1); ++j)
			{
				complex_double d = complex_double(0.0,0.0);
				for (int k=0; k<j; ++k) d += L(i,k)*L(j,k);
				L(i,j) = (i == j) ?
					sqrt(A(i,i) - d) :
					(1.0/L(j,j) * (A(i,j) - d));
			}
		return L;
	}
	else return A;
}

CMatrix diag(const CVector &v)
{
	CMatrix result(v.Size());
	for (int i=0; i<v.Size(); ++i) result(i,i) = v(i);
	return result;
}

CMatrix diagprod(const CVector &x, const CMatrix &B)
{
	CMatrix C(B.Rows(), B.Columns());
	if (x.Size() == B.Rows())
		for (int i=0; i<B.Rows(); ++i)
			for (int j=0; j<B.Columns(); ++j) C(i,j) = x(i)*B(i,j);
	return C;
}

CMatrix inverse(const CMatrix &A)
{
	CMatrix B = A;
	if (A.Rows() == A.Columns())
	{
		int s = A.Rows();
		Matrix M(2*s);
		for (int i=0; i<s; ++i)
			for (int j=0; j<s; ++j)
			{
				M(i,j) = A(i,j).real();
				M(i+s,j+s) = A(i,j).real();
				M(i,j+s) = A(i,j).imag();
				M(i+s,j) = -1.0*A(i,j).imag();
			}
		Matrix N = M.Inverse();
		for (int i=0; i<s; ++i)
			for (int j=0; j<s; ++j)
				B(i,j) = complex_double(N(i,j),N(i,j+s));
	}
	return B;
}

CMatrix zgemm(const complex_double alpha, const CMatrix &A, const CMatrix &B)
{
	CMatrix C(A.Rows(), B.Columns());
	if(A.Columns() == B.Rows())
		for (int i=0; i<A.Rows(); ++i)
			for (int j=0; j<B.Columns(); ++j)
				for (int k=0; k<A.Columns(); ++k)
					C(i,j) += alpha*A(i,k)*B(k,j);
	return C;
}

CMatrix zgemm(const complex_double alpha, const CMatrix &A, const CMatrix &B,
		const complex_double beta, CMatrix &C)
{
	if(A.Columns() == B.Rows())
		for (int i=0; i<A.Rows(); ++i)
			for (int j=0; j<B.Columns(); ++j)
			{
				C(i,j) *= beta;
				for (int k=0; k<A.Columns(); ++k)
					C(i,j) += alpha*A(i,k)*B(k,j);
			}
	return C;
}

CMatrix zgemmcpy(const complex_double alpha, const CMatrix &A, const CMatrix &B,
		const complex_double beta, const CMatrix &C)
{
	CMatrix result = C;
	if(A.Columns() == B.Rows())
		for (int i=0; i<A.Rows(); ++i)
			for (int j=0; j<B.Columns(); ++j)
			{
				result(i,j) *= beta;
				for (int k=0; k<A.Columns(); ++k)
					result(i,j) += alpha*A(i,k)*B(k,j);
			}
	return result;
}

CMatrix zgemm(const int m, const int n, const int o, const complex_double alpha,
		const CMatrix &A, const CMatrix &B)
{
	CMatrix C(m, n);
	for (int i=0; i<m; ++i)
		for (int j=0; j<n; ++j)
			for (int k=0; k<o; ++k)
				C(i,j) += alpha*A(i,k)*B(k,j);
	return C;
}

CMatrix zidm(const int s)
{
	CMatrix I(s);
	for (int i=0; i<s; ++i) I(i,i) = complex_double(1.);
	return I;
}

/*complex_double* conj(const int &m, complex_double *x)
{
	complex_double *y = new complex_double[m];
	for (int i=0; i<m; ++i) y[i] = conj(x[i]);
	return y;
	delete[] y;
	delete[] x;
}*/

complex_double* diagouter(complex_double **A, complex_double **B,
		const int &r, const int &c)
{
	complex_double *tmp = new complex_double[r];
	for (int i=0; i<r; ++i)
	{
		tmp[i] = 0.0;
		for (int j=0; j<c; ++j)
			tmp[i] += A[i][j]*std::conj(B[i][j]);
	}
	return tmp;
	delete[] tmp;
}

complex_double* zaxpy(const int &n, const complex_double &alpha,
		complex_double *x)
{
	complex_double *y = new complex_double[n];
	for (int i=0; i<n; ++i) y[i] = alpha*x[i];
	return y;
	delete[] x;
	delete[] y;
}

complex_double* zgemv(const int &m, const int &n, complex_double **A,
		complex_double *x)
{
	complex_double *y = new complex_double[n];

	for (int i=0; i<n; ++i)
	{
		y[i] = complex_double(0,0);
		for (int j=0; j<m; ++j) y[i] += A[i][j]*x[j];
	}
	return y;
}

complex_double **ccholesky(complex_double **A, const int &s)
{
	complex_double d;
	complex_double **L = new complex_double*[s];
	for (int i=0; i<s; ++i) L[i] = new complex_double[s];
	for (int i=0; i<s; ++i)
		for (int j=0; j<s; ++j) L[i][j] = 0.0;
	for (int i=0; i<s; ++i)
		for (int j=0; j<(i+1); ++j)
		{
			d = complex_double(0.0,0.0);
			for (int k=0; k<j; ++k) d += L[i][k]*L[j][k];
			L[i][j] = (i == j) ?
				sqrt(A[i][i] - d) :
				(1.0/L[j][j] * (A[i][j] - d));
		}
	return L;
	for (int i=0; i<s; ++i) delete[] A[i]; delete[] A;
	for (int i=0; i<s; ++i) delete[] L[i]; delete[] L;
}

complex_double** diagprod(const int &m, const int &n, complex_double *x,
		complex_double **B)
{
	complex_double **C = new complex_double*[m];
	for (int i=0; i<m; ++i) C[i] = new complex_double[n];
	for (int i=0; i<m; ++i)
		for (int j=0; j<n; ++j) C[i][j] = x[i]*B[i][j];
	return C;
	delete[] x;
	for (int i=0; i<m; ++i) delete[] B[i]; delete[] B;
	for (int i=0; i<m; ++i) delete[] C[i]; delete[] C;
}

complex_double **inverse(complex_double **m, const int &s)
{
	Matrix rm(2*s);
	for (int i=0; i<s; ++i)
		for (int j=0; j<s; ++j)
		{
			rm(i,j) = m[i][j].real();
			rm(i+s,j+s) = m[i][j].real();
			rm(i,j+s) = m[i][j].imag();
			rm(i+s,j) = -1.0*m[i][j].imag();
		}
	//rm.Print();
	Matrix rmi = rm.Inverse();
	//rmi.Print();

	complex_double **mi = new complex_double*[s];
	for (int i=0; i<s; ++i) mi[i] = new complex_double[s];
	for (int i=0; i<s; ++i)
		for (int j=0; j<s; ++j)
			mi[i][j] = complex_double(rmi(i,j),rmi(i,j+s));
	return mi;
	for (int i=0; i<s; ++i) delete[] m[i]; delete[] m;
	for (int i=0; i<s; ++i) delete[] mi[i]; delete[] mi;
}

complex_double **transpose(complex_double **m, const int &rows,
		const int &columns)
{
	complex_double **tmp = new complex_double*[columns];
	for (int i=0; i<columns; ++i) tmp[i] = new complex_double[rows];
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) tmp[j][i] = m[i][j];
	return tmp;
	for (int i=0; i<rows; ++i) delete[] m[i];
	delete[] m;
	for (int i=0; i<columns; ++i) delete[] tmp[i];
	delete[] tmp;
}

complex_double **conjTranspose(complex_double **m, const int &rows,
		const int &columns)
{
	complex_double **tmp = new complex_double*[columns];
	for (int i=0; i<columns; ++i) tmp[i] = new complex_double[rows];
	for (int i=0; i<rows; ++i)
		for (int j=0; j<columns; ++j) tmp[j][i] = std::conj(m[i][j]);
	//for (int i=0; i<rows; ++i) delete[] m[i]; delete[] m;
	return tmp;
	//for (int i=0; i<columns; ++i) delete[] tmp[i]; delete[] tmp;
}

/*complex_double** zidm(const int &m)
{
	complex_double **I = new complex_double*[m];
	for (int i=0; i<m; ++i) I[i] = new complex_double[m];
	for (int i=0; i<m; ++i)
		for (int j=0; j<m; ++j)
		{
			if (i == j) I[i][j] = complex_double(1.);
			else I[i][j] = complex_double(0.);
		}
	return I;
	for (int i=0; i<m; ++i) delete[] I[i];
	delete[] I;
}*/

complex_double** zgemm(const int &m, const int &n, const int &o,
		complex_double alpha, complex_double **A, complex_double **B)
{
	complex_double **C = new complex_double*[m];
	for (int i=0; i<m; ++i) C[i] = new complex_double[n];
	for (int i=0; i<m; ++i)
		for (int j=0; j<n; ++j)
		{
			C[i][j] = complex_double(0.0,0.0);
			for (int k=0; k<o; ++k)
				C[i][j] += alpha*A[i][k]*B[k][j];
		}
	return C;
	for (int i=0; i<m; ++i) delete[] A[i]; delete[] A;
	for (int i=0; i<m; ++i) delete[] B[i]; delete[] B;
	for (int i=0; i<m; ++i) delete[] C[i]; delete[] C;
}

complex_double** zgemm(const int &m, const int &n, const int &o,
		complex_double alpha, complex_double **A, complex_double **B,
		complex_double beta, complex_double **C)
{
	for (int i=0; i<m; ++i)
		for (int j=0; j<n; ++j)
		{
			C[i][j] *= beta;
			for (int k=0; k<o; ++k)
				C[i][j] += alpha*A[i][k]*B[k][j];
		}
	return C;
	for (int i=0; i<m; ++i) delete[] A[i]; delete[] A;
	for (int i=0; i<m; ++i) delete[] B[i]; delete[] B;
	for (int i=0; i<m; ++i) delete[] C[i]; delete[] C;
}

complex_double** zgemmcpy(const int &m, const int &n, const int &o,
		complex_double alpha, complex_double **A, complex_double **B,
		complex_double beta, complex_double **in)
{
	complex_double **out  = new complex_double*[m];
	for (int i=0; i<m; ++i) out[i] = new complex_double[n];
	for (int i=0; i<m; ++i)
		for (int j=0; j<n; ++j)
		{
			out[i][j] = in[i][j]*beta;
			for (int k=0; k<o; ++k)
				out[i][j] += alpha*A[i][k]*B[k][j];
		}
	return out;
	for (int i=0; i<m; ++i) delete[] A[i]; delete[] A;
	for (int i=0; i<m; ++i) delete[] B[i]; delete[] B;
	for (int i=0; i<m; ++i) delete[] in[i]; delete[] in;
	for (int i=0; i<m; ++i) delete[] out[i]; delete[] out;
}
