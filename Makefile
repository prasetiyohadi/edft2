edft2.x: edft2.cpp dft.o sch.o ewald.o poisson.o setup.o utils.o datatypes.o
	g++ -g -O3 -o edft2.x edft2.cpp dft.o sch.o ewald.o poisson.o setup.o utils.o datatypes.o -lfftw3 -lm

test: test.cpp setup.o utils.o datatypes.o
	g++ -g -O3 -o test.x test.cpp setup.o utils.o datatypes.o

test-parallel: test-parallel.cpp setup.o utils.o datatypes.o
	mpic++ -g -O3 -o test-parallel.x test-parallel.cpp setup.o utils.o datatypes.o

dft: dft.cpp dft.h
	g++ -g -c -o dft.o dft.cpp

sch: sch.cpp sch.h
	g++ -g -c -o sch.o sch.cpp

ewald: ewald.cpp ewald.h
	g++ -g -c -o ewald.o ewald.cpp

poisson: poisson.cpp poisson.h
	g++ -g -c -o poisson.o poisson.cpp

setup: setup.cpp setup.h
	g++ -g -c -o setup.o setup.cpp

utils: utils.cpp utils.h
	g++ -g -c -o utils.o utils.cpp

datatypes: datatypes.cpp datatypes.h
	g++ -g -c -o datatypes.o datatypes.cpp

object: dft sch ewald poisson setup utils datatypes

clean:
	rm *.o
