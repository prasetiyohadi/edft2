#ifndef _DFT_H
#define _DFT_H

#include "edft2.h"
#include "setup.h"
#include "utils.h"
#include "poisson.h"

struct DftSHM
{
	int ns, s;
	complex_double f;
	Vector V_shm;
	COperator op;
	DftSHM(const COperator &_op, const int &_Ns, const double &_f);
	double getE(const CMatrix &W) const;
	Vector excVWN(const Vector &n) const;
	Vector excpVWN(const Vector &n) const;
	CVector Vdual() const;
	CMatrix getGrad(const CMatrix &W) const;
	CMatrix H(const CMatrix &W) const;
	void fdtest(const CMatrix &W) const;
	void getPsi(const CMatrix &W, CMatrix &Psi, Vector &eps);
	void lm(CMatrix &W, Vector &E, const int N) const;
	void sd(CMatrix &W, const int t) const;
	void sd(CMatrix &W, Vector &E, const int N) const;
};

struct DftH
{
	int ns, s, Z;
	complex_double f;
	Vector G2;
	Matrix X;
	COperator op;
	DftH(const COperator &_op, const Matrix &_X, const int _Z, const int &_Ns,
			const double &_f);
	double getE(const CMatrix &W) const;
	Vector excVWN(const Vector &n) const;
	Vector excpVWN(const Vector &n) const;
	CVector Vdual() const;
	CVector Vps() const;
	CMatrix getGrad(const CMatrix &W) const;
	CMatrix H(const CMatrix &W) const;
	CMatrix K(const CMatrix &W) const;
	void fdtest(const CMatrix &W) const;
	void getPsi(const CMatrix &W, CMatrix &Psi, Vector &eps);
	void lm(CMatrix &W, Vector &E, const int N) const;
	void pccg(CMatrix &W, Vector &E, const int N, const int cg) const;
	void pclm(CMatrix &W, Vector &E, const int N) const;
	void sd(CMatrix &W, const int t) const;
	void sd(CMatrix &W, Vector &E, const int N) const;
};

#endif
