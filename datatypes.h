#ifndef _DATATYPES_H
#define _DATATYPES_H

#include "edft2.h"
#include <chrono>
#include <random>

// Types declaration
typedef std::complex<double> complex_double;

// Data types declarations
class Vector;
class Matrix;
class Tensor;
class CVector;
class CMatrix;

class Vector
{
    private:
        int size;
        double *data;
    public:
        Vector();
        Vector(int s);
        Vector(int s, const double a);
        Vector(int s, double *q);
        Vector(int s, const Vector &v);
        Vector(const Vector &v);
        Vector(int f, int d, const Matrix &m);
		template<int s1> Vector(double (&q)[s1]):Vector(s1)
		{
			for (int i=0; i<s1; ++i) data[i] = q[i];
		}
        ~Vector();

		double * GetPointer();
		double * GetPointer(int s1);
        void Initialize(int s);
        void Initialize(double a);
        void Initialize(double *v);
		void Resize(int s);
        Vector Offset(int offset);

        int Size() const;
		double MaxMod() const;
        double NormL1() const;
        double NormL2() const;
        double NormLinf() const;
        double Sum() const;
        double Product() const;
        double Length() const;
        double SquareLength() const;
        Vector Square() const;
        Vector Exp() const;

        Vector& operator=(const Vector &v);
        double operator()(const int i) const;
        double& operator()(const int i);

        void Print() const;
        void Normalize();
};

class Matrix
{
    private:
        int rows, columns;
        double **data;
    public:
        Matrix();
        Matrix(int s);
        Matrix(int r1, int c1);
        Matrix(int r1, int c1, const double a);
        Matrix(const Matrix &m);
        Matrix(int nv, const Vector *p);
        Matrix(int r1, int c1, double **q);
		template<int r1, int c1> Matrix(double (&q)[r1][c1]):
			Matrix(r1, c1)
		{
			for (int i=0; i<r1; ++i)
				for (int j=0; j<c1; ++j)
					data[i][j] = q[i][j];
		}
        ~Matrix();

        int Rows() const;
        int Columns() const;
        double ** GetPointer();
        double * GetPointer(int r1);
        void GetColumn(int c, Vector &x);
        void GetColumn(int c, Vector &x, int offset);
        void PutColumn(int c, const Vector &x);
		void Resize(int r1, int c1);
        void LUdecomp(Matrix &L, Matrix &U);
        void LUdecomp(Matrix &lu, Matrix &P, double &pv);
        void Solve(Vector &x, Vector &b);
        void Solve(Matrix &X, Matrix &B);

        double NormL1();
        double NormLinf();
		double Det();
        Matrix Inverse();

        Matrix& operator=(const Matrix &m);
        double operator()(const int i, const int j) const;
        double& operator()(const int i, const int j);

        Vector RowSum() const;
        Vector ColumnSum() const;
        Matrix Transpose() const;
        Matrix Square() const;

        void SwapRow(int r1, int r2);
        void Print() const;
        void Initialize(double a);
};

class DiagMatrix : public Matrix
{
	private:
		int rows, columns;
		double **data;
	public:
		DiagMatrix();
		DiagMatrix(int s);
        DiagMatrix(int r1, int c1);
		~DiagMatrix();

        int Rows() const;
        int Columns() const;
		void Resize(int r1, int c1);

        DiagMatrix& operator=(const DiagMatrix &m);
        double operator()(const int i, const int j) const;
        double& operator()(const int i, const int j);

        void Print() const;
};

class Tensor
{
    private:
        int x, y, z;
        double ***data;
    public:
        Tensor(int s);
        Tensor(int x1, int y1, int z1);
        Tensor(const Tensor &t);
        Tensor(int nm, const Matrix *p);
        Tensor(int nc, int nm, const Vector **q);
        Tensor(int x1, int y1, int z1, const Vector &v);
        Tensor(int x1, int y1, int z1, double ***r);
        ~Tensor();
        
        int X() const;
        int Y() const;
        int Z() const;

        Tensor& operator=(const Tensor &t);
        double operator()(const int i, const int j, const int k) const;
        double& operator()(const int i, const int j, const int k);

        void Print() const;
        void PrintXY() const;
        void PrintXZ() const;
        void PrintYZ() const;
};

class CVector
{
    private:
        int size;
        complex_double *data;
    public:
        CVector();
        CVector(int s);
        CVector(int s, const complex_double z);
        CVector(int s, const double a);
        CVector(int s, const double a, const double b);
        CVector(const CVector &v);
        CVector(const Vector &v);
        CVector(int s, const CVector &v);
        CVector(int s, complex_double *p);
        CVector(int f, int d, const CMatrix &m);
        CVector(int f, int d, const Matrix &m);
		template<int s1> CVector(complex_double (&q)[s1]):CVector(s1)
		{
			for (int i=0; i<s1; ++i) data[i] = q[i];
		}
        ~CVector();

        CVector &operator=(const CVector &v);
        CVector &operator=(const Vector &v);
        complex_double operator()(const int i) const;
        complex_double &operator()(const int i);

        int Size() const;
		double MaxMod() const;
        double NormL1() const;
        double NormL2() const;
        double NormLinf() const;
		complex_double *GetPointer();
		complex_double *GetPointer(int s1);
        complex_double IndefNormL2() const;
        void Print() const;
		CVector Conjugate() const;
		Vector Real() const;

        void Initialize(complex_double &z);
        void Initialize(double a);
		void Scale(complex_double &z);
		void Scale(double a);
        /*void Initialize(complex_double *p);
		void Resize(int s);*/
};

class CMatrix
{
    private:
        int rows, columns;
        complex_double **data;
    public:
        CMatrix();
        CMatrix(int s);
        CMatrix(int r1, int c1);
        CMatrix(int r1, int c1, const complex_double z);
        CMatrix(int r1, int c1, const double a);
        CMatrix(int r1, int c1, const double a, const double b);
        CMatrix(const CMatrix &m);
        CMatrix(const Matrix &m);
        CMatrix(int c, const CVector *p);
        CMatrix(int r1, int c1, complex_double **q);
		template<int r1, int c1> CMatrix(complex_double (&q)[r1][c1]):
			CMatrix(r1, c1)
		{
			for (int i=0; i<r1; ++i)
				for (int j=0; j<c1; ++j)
					data[i][j] = q[i][j];
		}
        ~CMatrix();

        CMatrix &operator=(const CMatrix &m);
        CMatrix &operator=(const Matrix &m);
        complex_double operator()(const int i, const int j) const;
        complex_double &operator()(const int i, const int j);

        complex_double **GetPointer();
        int Rows() const;
        int Columns() const;
        void Print() const;
		CMatrix Conjugate() const;
		CMatrix ConjTranspose() const;
		CMatrix Transpose() const;
		Matrix Real() const;

        void GetColumn(int c, CVector &v);
        void GetColumn(int c, CVector &v, int offset);
        void Initialize(complex_double z);
        void PutColumn(int c, const CVector &v);
		void Randomize();
		void Scale(complex_double &z);
		void Scale(double a);

        /*double ** GetPointer();
        void GetColumn(int c, Vector &x);
        void GetColumn(int c, Vector &x, int offset);
        void PutColumn(int c, const Vector &x);
		void Resize(int r1, int c1);
        void LUdecomp(Matrix &L, Matrix &U);
        void LUdecomp(Matrix &lu, Matrix &P, double &pv);
        void Solve(Vector &x, Vector &b);
        void Solve(Matrix &X, Matrix &B);

        double NormL1();
        double NormLinf();
		double Det();
        Matrix Inverse();

        Vector RowSum() const;
        Vector ColumnSum() const;
        Matrix Transpose() const;
        Matrix Square() const;

        void SwapRow(int r1, int r2);*/
};

// Operators declarations
Vector operator-(const Vector &v);
Vector operator+(const Vector &v1, const Vector &v2);
Vector operator-(const Vector &v1, const Vector &v2);
Vector operator*(const double &a, const Vector &v);
Vector operator*(const Vector &v, const double b);
Vector operator*(const Matrix &m, const Vector &v);
Vector operator*(const Vector &v, const Matrix &m);
Vector operator*(const DiagMatrix &m, const Vector &v);
Vector operator*(const Vector &v, const DiagMatrix &m);
Vector operator/(const Vector &v, const double &b);

Matrix operator-(const Matrix &m);
Matrix operator+(const Matrix &m1, const Matrix &m2);
Matrix operator-(const Matrix &m1, const Matrix &m2);
Matrix operator*(const Vector &v1, const Vector &v2);
Matrix operator*(const double &a, const Matrix &m);
Matrix operator*(const Matrix &m, const double &b);
Matrix operator*(const Matrix &A, const Matrix &B);
Matrix operator*(const DiagMatrix &A, const Matrix &B);
Matrix operator*(const Matrix &A, const DiagMatrix &B);
Matrix operator/(const Matrix &m, const double &b);

DiagMatrix operator*(const double &a, const DiagMatrix &m);
DiagMatrix operator*(const DiagMatrix &m, const double &b);
DiagMatrix operator/(const DiagMatrix &m, const double &b);

CVector operator+(const CVector &v1, const CVector &v2);
CVector operator-(const CVector &v1, const CVector &v2);
CVector operator*(const double &a, const CVector &v);
CVector operator*(const CVector &v, const double &b);
CVector operator/(const CVector &v, const double &b);

CMatrix operator-(const CMatrix &m);
CMatrix operator+(const CMatrix &m1, const CMatrix &m2);
CMatrix operator-(const CMatrix &m1, const CMatrix &m2);
CMatrix operator*(const double &a, const CMatrix &m);
CMatrix operator*(const CMatrix &m, const double &b);
CMatrix operator/(const CMatrix &m, const double &b);

complex_double* operator*(const DiagMatrix &m, const complex_double *c);

// Methods declarations
int minSize(const Vector &v1, const Vector &v2);
int minSize(const CVector &v1, const CVector &v2);
int minRows(const Matrix &m1, const Matrix &m2);
int minRows(const CMatrix &m1, const CMatrix &m2);
int minColumns(const Matrix &m1, const Matrix &m2);
int minColumns(const CMatrix &m1, const CMatrix &m2);

double dot(const Vector &v1, const Vector &v2);
double dot(const int n, const Vector &v1, const Vector &v2);
double inner(const Vector &v1, const Vector &v2);
double detDiagMat(const DiagMatrix &m);
double HouseholderTrans(Vector &x, Vector &w);
double randn();
double SecularEq(int N, double bm, double x, Vector &d, Vector &xi);
double SecularEq(double bm, int N, double *d, double *xi, double x);
double SecularEqImag(int N, complex_double bm, complex_double x, CVector &d,
		CVector &xi);
double SecularEqReal(int N, complex_double bm, complex_double x, CVector &d,
		CVector &xi);
double SecularEqDerivative(int N, double bm, double x, Vector &d, Vector &xi);
double SecularEqImagDImag(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi);
double SecularEqImagDReal(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi);
double SecularEqRealDImag(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi);
double SecularEqRealDReal(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi);
double Sign(const double &x);
double **CreateMatrix(int m, int n);

void DestroyMatrix(double ** mat, int m, int n);
void Eig(const Matrix &A, Matrix &V, Vector &lambda);
void Eig(const CMatrix &A, CMatrix &V, CVector &lambda);
void Hessenberg(Matrix &A);
void Hessenberg(CMatrix &A);
void Hessenberg(const CMatrix &A, CMatrix &P, CMatrix &H);
void Quicksort(double *p, double *v, int l, int r);
void SolveSecularEq(int N, double bm, Vector &d, Vector &xi, double *lambda);
void SolveSecularEq(int N, complex_double bm, CVector &d, CVector &xi,
		complex_double *lambda);
void SolveSecularEqBisec(int N, double bm, Vector &d, Vector &xi,
		double *lambda);
void SolveSecularEq(double bm, int N, double *d, double *xi, double * lambda);
void Swap(double &a, double &b);
void Swap(double *a, double *b);
void TDQREigensolver(int N, double *a, double *b, double *lambda, Matrix &Q);
void TDQREigensolver(int N, complex_double *a, complex_double *b,
		complex_double *lambda, CMatrix &Q);
void TDQREigensolver(int N, double *a, double *b, double *lambda, double **Q);
void TDQREigensolverBisec(int N, double *a, double *b, double *lambda,
		Matrix &Q);

Vector diagprod(const Vector &v1, const Vector &v2);
Vector diagVec(const Matrix &m);
Vector diagVec(const DiagMatrix &m);

Matrix curl(const Vector &v1, const Vector &v2);
DiagMatrix diagMat(const Vector &v);
DiagMatrix invDiagMat(const Vector &v);
DiagMatrix invDiagMat(const DiagMatrix &m);
Matrix outer(const Vector &v1, const Vector &v2);

//complex_double dot(const CVector &v1, const CVector &v2);
complex_double HouseholderTrans(int s, CVector &x, CVector &w);
complex_double SecularEq(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi);
complex_double SecularEqDerivative(int N, complex_double bm, complex_double x,
		CVector &d, CVector &xi);
complex_double Sign(const complex_double &z);
complex_double sum(const int &n, complex_double *x);
complex_double trace(const CMatrix &A);
complex_double trace(const int &m, const int &n, complex_double **A);
complex_double trace(const int m, const int n, const CMatrix &A);
complex_double zdotc(const int &s, complex_double *x, complex_double *y);
complex_double zdotc(const CVector &x, const CVector &y);
complex_double zdotc(const int s, const CVector &x, const CVector &y);
complex_double zdotu(const int s, CVector &x, CVector &y);

CVector diagouter(const CMatrix &A, const CMatrix &B);
CVector diagprod(const Vector &x, const CVector &z);
CVector diagprod(const CVector &v1, const CVector &v2);
CVector zgbmv(const int m, const int n, const complex_double alpha,
		const CMatrix &A, const CVector &x);
CVector zgbvv(const int m, const int n, const complex_double alpha,
		const CVector &x, const CVector &y);

CMatrix ccholesky(const CMatrix &A);
CMatrix diag(const CVector &v);
CMatrix diagprod(const CVector &x, const CMatrix &B);
CMatrix inverse(const CMatrix &A);
CMatrix zgemm(const complex_double alpha, const CMatrix &A, const CMatrix &B);
CMatrix zgemm(const complex_double alpha, const CMatrix &A, const CMatrix &B,
		const complex_double beta, CMatrix &C);
CMatrix zgemmcpy(const complex_double alpha, const CMatrix &A, const CMatrix &B,
		const complex_double beta, const CMatrix &C);
CMatrix zgemm(const int m, const int n, const int o, const complex_double alpha,
		const CMatrix &A, const CMatrix &B);
CMatrix zidm(const int s);

//complex_double* conj(const int &m, complex_double *x);
complex_double* diagouter(complex_double **A, complex_double **B,
		const int &r, const int &c);
complex_double* zaxpy(const int &n, const complex_double &alpha,
		complex_double *x);
complex_double* zgemv(const int &m, const int &n, complex_double **A,
		complex_double *x);

complex_double **ccholesky(complex_double **A, const int &s);
complex_double** diagprod(const int &m, const int &n, complex_double *x,
		complex_double **B);
complex_double** inverse(complex_double **m, const int &s);
complex_double** transpose(complex_double **m, const int &rows,
		const int &columns);
complex_double** conjTranspose(complex_double **m, const int &rows,
		const int &columns);
//complex_double** zidm(const int &m);
complex_double** zgemm(const int &m, const int &n, const int &o,
		complex_double alpha, complex_double **A, complex_double **B);
complex_double** zgemm(const int &m, const int &n, const int &o,
		complex_double alpha, complex_double **A, complex_double **B,
		complex_double beta, complex_double **C);
complex_double** zgemmcpy(const int &m, const int &n, const int &o,
		complex_double alpha, complex_double **A, complex_double **B,
		complex_double beta, complex_double **in);

#endif
