#include "edft2.h"
#include "utils.h"
#include "setup.h"
#include "poisson.h"
#include "ewald.h"
#include "sch.h"
#include "dft.h"

int main()
{
    // define sampling as 3x1 vector
	Vector S(3);
	//S(0) = 3; S(1) = 4; S(2) = 5;
	//S(0) = 20; S(1) = 25; S(2) = 30;
	//S(0) = 32; S(1) = 32; S(2) = 32;
	S(0) = 64; S(1) = 64; S(2) = 64;

    // define lattice vector as 3x3 matrix
	int dim = S.Size();
    Matrix R(dim);
    //R(0,0) = 6; R(1,1) = 6; R(2,2) = 6;
    R(0,0) = 16; R(1,1) = 16; R(2,2) = 16;

	// initialize Index object to store sampling points
	Index idx(R, S);
	std::cout << "total sampling = " << idx.s
		<< ", dimension/rank = " << idx.dim << "\n";

	// plot sampling point representation
	//set_dplot_view(idx.G2, S);

	// define gaussian parameter to simulate electron density
	double sigma1 = 0.75;
	double sigma2 = 0.50;
	// initialize Density object to simulate electron density
	Density rho(idx, sigma1, sigma2);

	std::cout << "Normalization check on g1: " << 
		rho.g1.Sum()*R.Det()/S.Product() << "\n";
	std::cout << "Normalization check on g2: " << 
		rho.g2.Sum()*R.Det()/S.Product() << "\n";
	std::cout << "Total charge check: " << 
		rho.n.Sum()*R.Det()/S.Product() << "\n";

	// plot gaussian representation of electron density
	//set_dplot_gaussian(rho.n, S);

	// initialize basis set dependent complex operator object
	COperator op(idx);

	// define number of states
	int Ns = 1;
	//int Ns = 4;
	// initialize electron density as matrix
	Matrix n_mat(idx.s, Ns);
	n_mat.PutColumn(0, rho.n);

	// initialize Poisson object to simulate Poisson equation
	Poisson poisson(op);

	std::cout << "Numeric, analytic Coulomb energy: " 
		<< poisson.Unum(rho.n) << ", " 
		<< rho.Uanal() << "\n";

	std::cout.precision(17);
	std::cout << "Numeric, analytic Coulomb energy: " 
		<< std::fixed 
		<< poisson.Unum(n_mat) << ", " 
		<< rho.Uanal() << "\n";

	// plot potential solution from Poisson equation
	//set_dplot_phi(poisson.rphi, S);

	// define atomic locations and nuclear charge
	Matrix X(2, 3); X(1,0) = 1.5;
	int Z = 1;

	// initialize gaussian parameter to simulate nuclear potential
	double ewald_sigma = 0.25;
	// initialize Density object to simulate nuclear potential
	Density ewald_rho(idx, Z, ewald_sigma);
	// initialize Ewald object to simulate solution of Poisson like equation
	// to calculate nuclear potential
	Ewald ewald(op, X);

	double U_ewald_num = ewald.Unum(ewald_rho.g);
	double U_ewald_self = ewald_rho.Uself(X);
	std::cout << "Total numeric, self Ewald energy: " 
		<< U_ewald_num << ", " 
		<< U_ewald_self << "\n";
	std::cout << "Nett Ewald energy: " << U_ewald_num - U_ewald_self << "\n";


	// Schroedinger solver
	// DFT solver
	//Sch sch(op, Ns);
	//double f = 1.;
	double f = 2.;
	//DftSHM dftshm(op, Ns, f);
	DftH dfth(op, X, Z, Ns, f);
	int s = S.Product(), ns = Ns;
	CMatrix W(s, ns); W.Randomize();
	//double E = dftshm.getE(W);
	double E = dfth.getE(W);
	std::cout << "The calculated energy is : " << E << "\n";

	// finite different test
	//sch.fdtest(W);
	//dftshm.fdtest(W);
	dfth.fdtest(W);
	
	CMatrix W2 = zgemm(complex_double(1.,0.), W.ConjTranspose(), op.O(W));
	W = zgemm(complex_double(1.,0.), W, inverse(ccholesky(W2)));

	// steepest descend
	//sch.sd(W, 400);
	Vector Elist(1000);
	//dftshm.sd(W, 50);
	//dftshm.sd(W, Elist, 1000); 
	dfth.sd(W, 20);
	//dfth.sd(W, Elist, 1000); 
	
	W2 = zgemm(complex_double(1.,0.), W.ConjTranspose(), op.O(W));
	W = zgemm(complex_double(1.,0.), W, inverse(ccholesky(W2)));

	//dftshm.lm(W, Elist, 1000); 
	//dfth.lm(W, Elist, 1000); 
	//dfth.pclm(W, Elist, 1000); 
	dfth.pccg(W, Elist, 1000, 1); 
	//dfth.pccg(W, Elist, 1000, 2); 
	//dfth.pccg(W, Elist, 1000, 3); 
	Elist.Print(); std::cout << "Elist size = " << Elist.Size() << "\n";

	//W2 = zgemm(complex_double(1.,0.), W.ConjTranspose(), op.O(W));
	//W = zgemm(complex_double(1.,0.), W, inverse(ccholesky(W2)));
	
	// state energy
	//CMatrix Psi(ns);
	//Vector eps(ns);
	//sch.getPsi(W, Psi, eps);
	//dftshm.getPsi(W, Psi, eps);
	//Psi.Print();
	//eps.Print();

    return 0;
}
