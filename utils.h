#ifndef _UTILS_H
#define _UTILS_H

#include "edft2.h"
#include "datatypes.h"
#include <chrono>

double doubleEps();
Matrix set_slice(const Vector& dat, const Vector& S, int l, int m, int *r,
		int *c);
Matrix randn(const int &m, const int &n);
void set_dplot_view(const Vector &dat, const Vector &S);
void set_dplot_gaussian(const Vector &dat, const Vector &S);
void set_dplot_phi(const Vector &dat, const Vector &S);

std::string num_to_str(int n);
std::string num_to_str(double n);
void out_a(const Vector &A);
void out_m(const Matrix& M);
void fout_a(const Vector &A);
void fout_m(const Matrix& M, int r, int c);
void fout_m(const Matrix& M, std::string name, int r, int c);
void fout_m(const Matrix& M, std::string name, int n, int r, int c);

#endif
